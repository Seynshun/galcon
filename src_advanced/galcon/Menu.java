package galcon;

import javafx.scene.Group;
import javafx.scene.control.Button;

/**
 * Create the game menu with 3 buttons:
 * <ul>
 *
 * <li> Play </li>
 * <li> AI vs AI </li>
 * <li> Load </li>
 * </ul>
 * Does not manage the actions of the buttons.
 */
public class Menu {
	
	/**
	 * Root of the game
	 */
	Group root;
	/**
	 * button AI vs AI
	 */
	Button ai;
	/**
	 * button Play
	 */
	Button play;
	/**
	 * button for the 2-player mode
	 */
	Button twoPlayers;
	/**
	 * button for the 3-player mode
	 */
	Button threePlayers;
	/**
	 * button for the 4-player mode
	 */
	Button fourPlayers;
	/**
	 * button for the AI vs AI mode (two AI)
	 */
	Button twoAi;
	/**
	 * button for the AI vs AI mode (three AI)
	 */
	Button threeAi;
	/**
	 * button for the AI vs AI mode (four AI)
	 */
	Button fourAi;
	/**
	 * button Load
	 */
	Button load;
	
	
	
	/**
	 * Menu creator, creates buttons.
	 * 
	 * @param root Root of the game to display the buttons
	 * @param width The width of the window
	 */
	public Menu(Group root, int width) {
		this.root = root;
		
		int marge = 200; 
		
		ai = new Button("AI vs AI");
		ai.setMinSize(200,50);
		ai.setTranslateX(width/2 - 100);
		ai.setTranslateY(marge + 60);
		
		twoAi = new Button("2-AI mode");
		twoAi.setMinSize(200,50);
		twoAi.setTranslateX(width/2 - 100);
		twoAi.setTranslateY(marge);
		
		threeAi = new Button("3-AI mode");
		threeAi.setMinSize(200,50);
		threeAi.setTranslateX(width/2 - 100);
		threeAi.setTranslateY(marge+60);
		
		fourAi = new Button("4-AI mode");
		fourAi.setMinSize(200,50);
		fourAi.setTranslateX(width/2 - 100);
		fourAi.setTranslateY(marge+120);
		
		play = new Button("Play");
		play.setMinSize(200,50);
		play.setTranslateX(width/2 - 100);
		play.setTranslateY(marge);
		
		twoPlayers = new Button("2-player mode");
		twoPlayers.setMinSize(200,50);
		twoPlayers.setTranslateX(width/2 - 100);
		twoPlayers.setTranslateY(marge);
		
		threePlayers = new Button("3-player mode");
		threePlayers.setMinSize(200,50);
		threePlayers.setTranslateX(width/2 - 100);
		threePlayers.setTranslateY(marge+60);
		
		fourPlayers = new Button("4-player mode");
		fourPlayers.setMinSize(200,50);
		fourPlayers.setTranslateX(width/2 - 100);
		fourPlayers.setTranslateY(marge+120);
		
		load = new Button("Load");
		load.setMinSize(200,50);
		load.setTranslateX(width/2 - 100);
		load.setTranslateY(marge + 120);
		
		
	}
	
	/**
	 * Display the buttons
	 */
	public void showMenu() {
		root.getChildren().add(ai);
		root.getChildren().add(load);
		root.getChildren().add(play);
	}
	/**
	 * Hide the buttons
	 */
	public void hideMenu() {
		root.getChildren().remove(ai);
		root.getChildren().remove(play);
		root.getChildren().remove(load);
	}
	/**
	 * Display this buttons when you click on the play button
	 */
	public void showMenuPlay() {
		root.getChildren().add(twoPlayers);
		root.getChildren().add(threePlayers);
		root.getChildren().add(fourPlayers);
	}
	/**
	 * Hide the buttons display by {@link #showMenuPlay}
	 */
	public void hideMenuPlay() {
		root.getChildren().remove(twoPlayers);
		root.getChildren().remove(threePlayers);
		root.getChildren().remove(fourPlayers);
	}
	/**
	 * Display this buttons when you click on the AI vs AI button
	 */
	public void showMenuAI() {
		root.getChildren().add(twoAi);
		root.getChildren().add(threeAi);
		root.getChildren().add(fourAi);
	}
	/**
	 * Hide the buttons display by {@link #showMenuAI()}
	 */
	public void hideMenuAi() {
		root.getChildren().remove(twoAi);
		root.getChildren().remove(threeAi);
		root.getChildren().remove(fourAi);
	}
	
	/* ===================================== */
	/* ==========GETTER AND SETTER========== */
	/* ===================================== */
	
	

	public Button getAi() {
		return ai;
	}

	public Button getPlay() {
		return play;
	}

	public Button getLoad() {
		return load;
	}

	public Button getTwoPlayers() {
		return twoPlayers;
	}

	public Button getThreePlayers() {
		return threePlayers;
	}

	public Button getTwoAi() {
		return twoAi;
	}

	public Button getThreeAi() {
		return threeAi;
	}

	public Button getFourPlayers() {
		return fourPlayers;
	}

	public Button getFourAi() {
		return fourAi;
	}
	
}
