package galcon;

import java.util.ArrayList;

import javafx.scene.Group;
import javafx.scene.input.MouseEvent;

/**
 * Manages mouse interactions.
 * 
 * <ul>
 * <li>
 * When the player clicks on a planet of his own, it is selected as a source.
 * Then the player slides the mouse over a destination planet.
 * </li>
 * <li>
 * Releasing the mouse on a planet other than the starting planet
 * selects it as the destination planet.
 * </li>
 * <li>
 * The player can click/drop several planets he has on a source planet to send
 * squadrons from all selected planets at the same time.
 * </li>
 * <li>
 * A click in the void or on a planet selected as a source will cancel the mission.
 * </li>
 * </ul>
 *
 */
public class Mousehandler {
	/**
	 * ID number of the player.
	 */
	private int player;
	
	
	
	
	/**
	 * 
	 * Contains the player's ID number (to know which planet belongs to him).
	 * 
	 * @param player ID number of player
	 */
	public Mousehandler(int player) {
		this.player = player;
	}
	
	
	
	/**
	 * Action to be performed when sliding the mouse.
	 * 
	 * <ul>
	 * <li>
	 * If one or more planets are selected, a line will be drawn
	 * between these planets and the cursor.
	 * </li>
	 * <li>
	 * If a planet is selected any other planet that
	 * the mouse hovers over will be selected as a destination.
	 * </li>
	 * </ul>
	 * 
	 * @param tabPlanets All the planets in the game
	 * @param e Mouse event
	 * @param root Root of the game
	 * 
	 */
	public void mouseDragged(ArrayList<Planet> tabPlanets, MouseEvent e, Group root) {
		// determines if a planet is selected as a source
		boolean selectedsrc = false;
		for(Planet p : tabPlanets) {
			if(p.isSelectedsrc()) {
				selectedsrc = true;
				break;
			}
		}
		for(Planet p : tabPlanets) {
			if(selectedsrc) {
				if(p.isInside(e) && !p.isSelectedsrc()) {
					// selected destination planet
					p.setSelecteddst(true);
				}
				else if(!p.isSelectedsrc()){
					// deselect other planets
					p.setSelecteddst(false);
				}
				else if(p.isSelectedsrc()) {
					// the source planet draws the line
					p.drawLine(e, root);
				}
			}
		}
	}
	
	
	
	/**
	 * Action to be performed when the mouse is released.
	 * 
	 * <ul>
	 * <li>
	 * If no planet is selected as the destination when the mouse is released,
	 * all source planets are deselected.
	 * </li>
	 * <li>
	 * With a destination, the line of source planets are centered on the
	 * destination planet and a ship is added to the take-off run-way.
	 * </li>
	 * </ul>
	 * 
	 * @param tabPlanets All the planets in the game
	 * @param e Mouse event
	 * @param root Root of the game
	 */
	public void mouseReleased(ArrayList<Planet> tabPlanets, MouseEvent e, Group root) {
		// find the destination planet
		Planet planetdst = null;
		for(Planet p : tabPlanets) {
			if(p.isSelecteddst()) {
				planetdst = p;
			}
		}
		
		for(Planet p : tabPlanets) {
			// We deselect the source planet by canceling the mission
			if(p.isSelectedsrc() && planetdst == null) {
				p.setSelectedsrc(false);
				p.deleteLine(root);
				p.dismissAllUnits();
			}
			// We add a ship to the mission
			else if(p.isSelectedsrc() && planetdst != null) {
				p.drawLine(planetdst, root);
				if(p.getUnits() == 0) {
					p.hireUnit();
				}
			}
		}
	}
	
	/**
	 * Action to be performed when clicking.
	 * 
	 * <ul>
	 * <li>
	 * If the planet that is clicked belongs to the player it is selected as the source.
	 * </li>
	 * <li>
	 * If a source and destination planet are already selected,
	 * clicking on another planet changes the destination.
	 * </li>
	 * </ul>
	 * 
	 * @param tabPlanets All the planets in the game
	 * @param e Mouse event
	 * @param root Root of the game
	 */
	public void mousePressed(ArrayList<Planet> tabPlanets, MouseEvent e, Group root) {
		
		
		if(e.isControlDown()){
			for(Planet p : tabPlanets) {
				if(!p.isInside(e) && p.getPlayer() == player) {
					p.setSelectedsrc(true);
					p.setSelecteddst(false);
					p.drawLine(e, root);
				}
				else if(p.isInside(e)){
					p.setSelecteddst(true);
				}
			}
		}
		else if(e.isShiftDown()) {
			for(Planet p : tabPlanets) {
				if(p.isInside(e) && p.getCurrentPlayer() == player) {
					p.switchSpaceShips();
				}
			}
		}
		else {
			// look for a source planet
			boolean issrc = false;
			for(Planet p : tabPlanets) {
				if(p.isSelectedsrc()) {
					issrc = true;
					break;
				}
			}
			for(Planet p : tabPlanets) {
				// draw the source planets line
				if(p.isSelectedsrc()) {
					p.drawLine(e, root);
				}
				if(p.isInside(e)) {
					// the player click on a planet of his own
					if(p.getPlayer() == player){
						p.setSelectedsrc(true);
						p.setSelecteddst(false);
						p.drawLine(e, root);
					}
					// the player select a destination
					else if(issrc && ! p.isSelecteddst()){
						p.setSelecteddst(true);
					}
				}
				else{ // if the player click on nothing we deselect the destination
					p.setSelecteddst(false);
				}
			}
		}
	}

	
	
	/* ===================================== */
	/* ==========GETTER AND SETTER========== */
	/* ===================================== */
	
	
	
	public void setPlayer(int player) {
		this.player = player;
	}

	public int getPlayer() {
		return player;
	}
}
