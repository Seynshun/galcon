package galcon;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 * Class for square planets.
 * Attacks deal twice more damage and spaceship production is twice faster.
 *
 */
public class PlanetSquare extends Planet{
	
	/**
	 * Serial version for serialisation
	 */
	private static final long serialVersionUID = 1L;

	
	
	/**
	 * Empty constructor
	 */
	public PlanetSquare() {}
	
	
	
	/**
	 * Construct a square planet using super constructor
	 * {@link Planet#Planet(String, double, Shape, Group, int, int, int , int)}
	 * 
	 * @see Planet#Planet(String, double, Shape, Group, int, int, int , int)
	 * @param path  Path of image
	 * @param size Size of image
	 * @param shape Shape for collision
	 * @param root Root of the game
	 * @param player1 Player 1
	 * @param player2 Player 2
	 * @param player3 Player 3
	 * @param player4 Player 4
	 */
	public PlanetSquare(String path, double size, Shape shape, Group root, int player1, int player2, int player3, int player4) {
		super(path, size, shape, root, player1, player2, player3, player4);
		
	}
	
	
	
	/**
	 * Override the function {@link Planet#attack(double)}
	 * to deal twice more damage
	 */
	@Override
	public void attack(double power) {
		super.attack(power*2);
	}
	
	
	/**
	 * Override the function {@link Planet#update()}
	 * to make production time twice faster
	 */
	@Override
	public void update() {
		setCurrentTime(getCurrentTime()+1);
		super.update();
	}
	
	/**
	 * Override {@link Planet#moveSpaceships()}
	 * to make the spaceship turn around the square
	 */
	@Override
	public void moveSpaceships() {
		double marge = 10; // distance spaceship - planet
		double rotateSpeed = 0.3; // rotation speed		
		
		double distance = (getCurrentAngle() + rotateSpeed)%(4*(getSize()+marge*2));
		
		setCurrentAngle(distance);
		
		for(SpaceShip s : getSpaceships()) {
			// spaceship rotation
			if(!s.isLaunched() && s.getTrajectory().size()==0) {
				
				int ind = getSpaceships().indexOf(s);
				
				Rectangle rec = (Rectangle)getShape();
				
				double size = rec.getWidth();
				
				double X = rec.getX();
				double x = X - marge + distance - s.getSize()*ind;
				
				double Y = rec.getY();
				double y = Y - marge;
				
				// place the planet around the square according it's index
				while(x < X-marge || x > X+size+marge || y < Y-marge || y > Y+size+marge) {
					if(x == X-marge){
						if(y < Y-marge) {
							x = x + (Y-marge)-y;
							y = Y-marge;
						}
						else if(y > Y+size+marge){
							x = x + (y-(Y+size+marge));
							y = Y+size+marge;
						}
					}
					
					if(x == X+size+marge) {
						if(y < Y-marge) {
							x = x - ((Y-marge)-y);
							y = Y-marge;
						}
						else if(y > Y+size+marge){
							x = x - (y-(Y+size+marge));
							y = Y+size+marge;
						}
					}
					
					if(y == Y-marge) {
						if(x < X-marge) {
							y = y + ((X-marge)-x); 
							x = X-marge;
						}
						else if(x > X+size+marge){
							y = y + (x-(X+size+marge));
							x = X+size+marge;
						}
					}

					if(y == Y+size+marge) {
						if(x < X-marge) {
							y = y - ((X-marge)-x);
							x = X-marge;
						}
						else if(x > X+size+marge){
							y = y - (x-(X+size+marge));
							x = X+size+marge;
						}
					}
						
				}
				s.setPosition(new Point2D(x-s.getSize()/2,y-s.getSize()/2));
			}
			// position of a spaceship launched
			else if(s.isLaunched() && getLine() != null) {
				int ind = getSpaceships().indexOf(s)+1;
				s.setLaunchPosition(ind);
			// position of a spaceship sent determined in 
			}
		}
		
		ArrayList<SpaceShip> copyShipSent = new ArrayList<SpaceShip>();
		copyShipSent.addAll(getShipSent());
		for(SpaceShip s : copyShipSent) {
			s.updatePosition();
			if(s.isArrived()) {
				getShipSent().remove(s); // remove the spaceship when arrived
			}
		}
	}
	
	
	
	
	// TODO
		public void sendWave() {
			setWaveTimer(1);
			
			double shipsize = getSpaceships().get(0).getSize();
			
			// find the number of spaceship to send
			int nbToSend;
			// send a complete wave if there is too many ships to send
			double R = Math.sqrt(2)*(getSize()/2 +10);
			if(getUnits()*shipsize >= 2*Math.PI*R) {
				nbToSend = (int)((2*Math.PI*R)/shipsize);
				setUnits(getUnits() - nbToSend);
			}
			// send all spaceships if it is possible
			else {
				nbToSend = getUnits();
				setUnits(0);
			}
			
			// send the wave of spaceships
			for(int i=0; i<nbToSend; i++) {
				SpaceShip s = getSpaceships().get(i);
				
				if(s.getPlanetdst() != null) {
					// take back the destination of the spaceship
					Planet dst = s.getPlanetdst();
					
					ArrayList<Segment> tabseg = new ArrayList<>();
					Line l;
					// if the destination is a square : use end point as the center of the square
					if(dst instanceof PlanetSquare) {
						Rectangle rec = (Rectangle)dst.getShape();
						l= new Line(s.getPosition().getX() + s.getSize()/2, s.getPosition().getY() + s.getSize()/2,
								 rec.getX()+rec.getWidth()/2, rec.getY()+rec.getWidth()/2);
					}
					else {
						Circle c = (Circle)dst.getShape();
						// original path before cutting
						l= new Line(s.getPosition().getX() + s.getSize()/2, s.getPosition().getY() + s.getSize()/2,
										 c.getCenterX(), c.getCenterY());
					}
					
					tabseg.add(new Segment(l));
					// cutting the trajectory
					s.setTrajectory(s.findPath(getAllPlanets(), tabseg, dst));
					// the ship is sent only if it founds a trajectory, otherwise it is destroyed
					if(s.getTrajectory().size() != 0) {
						getShipSent().add(s); // adding ships to the sent ships array
					}
				}
			}
			
			removeAllSentShip(nbToSend);
			
			if(getUnits() == 0) {
				setWaveTimer(0);
			}
		}
	
	
	
	/**
	 * Override {@link Planet#drawText(Group)}.
	 * Find the number of ships to print with a square planet
	 */
	public void drawText(Group root) {
		String nb;
		root.getChildren().remove(getLabel());
		setLabel(new Label());
		
		int total = getSpaceships().size();
		double shipsize = getSpaceships().get(0).getSize();
		
		// if units is too big we print it
		double R = Math.sqrt(2)*(getSize()/2 +10);
		if(getUnits()*shipsize >= 2*Math.PI*R) {
			nb = String.valueOf(getUnits());
			getLabel().setTextFill(Color.RED);
		}
		// else we print the total of ships minus units
		else if((total-getUnits())*shipsize >= (getSize()+20)*4){
			nb = String.valueOf(total-getUnits());
			getLabel().setTextFill(Color.DARKBLUE);
		}
		else {
			nb = "";
		}
		
		getLabel().setText(nb);
		getLabel().setFont(Font.font("Helvetica", FontWeight.BOLD, 20));
		
		getLabel().setPrefSize(100, 50);
		getLabel().setAlignment(Pos.CENTER);
		getLabel().setTranslateX(getPosition().getX() + getSize()/2 - getLabel().getPrefWidth()/2);
		getLabel().setTranslateY(getPosition().getY() + getSize()/2 - getLabel().getPrefHeight()/2);
		
		root.getChildren().add(getLabel());
		
	}
	
	
	
	/**
	 * Override {@link Planet#render(GraphicsContext, Group)}
	 * to show the squares shapes, the lines, and the numbers of ships
	 */
	public void render(GraphicsContext gc, Group root) {
		// render all spaceships
		for(SpaceShip s : getSpaceships()) {
			if(s.getPosition() != null) {
				
				// units ready for mission
				double R = Math.sqrt(2)*(getSize()/2 +10);
				if(getSpaceships().indexOf(s) < getUnits() &&
				   (getSpaceships().indexOf(s)+1)*s.getSize() <= 2*Math.PI*R)
				{
					s.render(gc);
				}
				// spaceship in orbital
				else if((getSpaceships().indexOf(s) >= getUnits() && 
						 (getSpaceships().indexOf(s)-getUnits()+1)*s.getSize() <= (getSize()+20)*4) )
				{
					s.render(gc);
				}
			}
		}
		
		// print ships number
		for(SpaceShip s : getShipSent()) {
			if(s.getPosition() != null) {
				s.render(gc);
			}
		}
		
		// draw number of spaceships if needed
		if(getSpaceships().size() != 0) {
			drawText(root);
		}
		
		// render picture of the planet
		gc.drawImage(super.getImage(), super.getPosition().getX(), super.getPosition().getY());
		Rectangle rec = (Rectangle)super.getShape(); // creating shape of the planet
		rec.setStrokeWidth(2);
		rec.setFill(null);
		
		if(getLine() != null) {
			root.getChildren().remove(getLine());
			// show lines
			if(getCurrentPlayer() == getPlayer1()) {
				getLine().setStroke(Color.MAGENTA);
			}
			else if(getCurrentPlayer() == getPlayer2()){
				getLine().setStroke(Color.RED);
			}
			else if(getCurrentPlayer() == getPlayer3()) {
				getLine().setStroke(Color.WHITESMOKE);
			}
			else if(getCurrentPlayer() == getPlayer4()) {
				getLine().setStroke(Color.ORANGE);
			}
			root.getChildren().add(getLine());
		}
		
		if(isSelectedsrc()) { // color for source
			rec.setStroke(Color.YELLOW);
			root.getChildren().remove(rec);
			root.getChildren().add(super.getShape());
		}
		else if(isSelecteddst()) { // color for destination
			rec.setStroke(Color.LAWNGREEN);
			root.getChildren().remove(rec);
			root.getChildren().add(super.getShape());
		}
		else {
			if(this.getCurrentPlayer() == getPlayer1()) { // color player1
				rec.setStroke(Color.MAGENTA);
				root.getChildren().remove(rec);
				root.getChildren().add(super.getShape());
			}
			else if(this.getCurrentPlayer() == getPlayer2()) { // color player2
				rec.setStroke(Color.RED);
				root.getChildren().remove(rec);
				root.getChildren().add(super.getShape());
			}
			else if(this.getCurrentPlayer() == getPlayer3()) { // color joueur3
				rec.setStroke(Color.WHITESMOKE);
				root.getChildren().remove(rec);
				root.getChildren().add(super.getShape());
			}
			else if(this.getCurrentPlayer() == getPlayer4()) { // color joueur4
				rec.setStroke(Color.ORANGE);
				root.getChildren().remove(rec);
				root.getChildren().add(super.getShape());
			}
			else {
				rec.setStroke(Color.BLUE); // color free planet
				root.getChildren().remove(rec);
				root.getChildren().add(super.getShape());
			}
		}
	}
	
	
	
	/**
	 * Override {@link Planet#isInside(MouseEvent)}
	 * To select the planet when the mouse is on it
	 */
	public boolean isInside(MouseEvent e) {
		double x = e.getX();
		double y = e.getY();
		
		Rectangle rec = (Rectangle) getShape();
		double X = rec.getX();
		double Y = rec.getY();
		double size = rec.getWidth();
		
		return x >= X && x <= X+size && y >= Y && y <= Y+size;
	}
	
	
	
	/**
	 * Override {@link Planet#isInside(Point2D)}
	 * to know if a point is inside the square planet
	 */
	public boolean isInside(Point2D p) {
		double x = p.getX();
		double y = p.getY();
		
		Rectangle rec = (Rectangle) getShape();
		double X = rec.getX();
		double Y = rec.getY();
		double size = rec.getWidth();
		
		return x >= X && x <= X+size && y >= Y && y <= Y+size;
	}
	
	
	
	/**
	 * Override {@link Planet#isInside(Point2D, double)}
	 * to determine the way point for the spaceships to avoid the 
	 * planets.
	 */
	public boolean isInside(Point2D p, double marge) {
		// reduce the margin to make the distance from the center of the
		// extended square to its corner equals to half the size + the margin
		marge = marge/Math.sqrt(2);
		
		double x = p.getX();
		double y = p.getY();
		
		Rectangle rec = (Rectangle) getShape();
		double X = rec.getX();
		double Y = rec.getY();
		double size = rec.getWidth();
		
		return x >= X - marge && x <= X+size+marge && y >= Y-marge && y <= Y+size+marge;
	}
	
	
	/**
	 * Override {@link Planet#drawLine(Planet, Group)}
	 * to draw the line from the center of the square planet
	 * to the planet p2
	 */
	public void drawLine(Planet p2, Group root) {
		root.getChildren().remove(getLine());
		Rectangle rec = (Rectangle)getShape();
		if(p2 instanceof PlanetSquare) {
			Rectangle rec2 = (Rectangle)p2.getShape();
			setLine(new Line(rec.getX()+rec.getWidth()/2, rec.getY()+rec.getWidth()/2,
							 rec2.getX()+rec2.getWidth()/2, rec2.getY()+rec2.getWidth()/2));
		}
		else {
			Circle c2 = (Circle)p2.getShape();
			setLine(new Line(rec.getX()+rec.getWidth()/2, rec.getY()+rec.getWidth()/2,
					c2.getCenterX(), c2.getCenterY()));
		}
	}
	
	
	
	/**
	 * Override {@link Planet#drawLine(MouseEvent, Group)}
	 * to draw the line from the center of the square planet
	 * to the mouse
	 */
	public void drawLine(MouseEvent e, Group root) {
		root.getChildren().remove(getLine());
		Rectangle rec = (Rectangle)getShape();
		setLine(new Line(rec.getX()+rec.getWidth()/2, rec.getY()+rec.getWidth()/2, e.getX(), e.getY()));
	}
	
	
	/**
	 * Override {@link Planet#save(ObjectOutputStream)}
	 * to save the planet using 1 to restore it as a
	 * square planet
	 */
	public void save(ObjectOutputStream os) throws IOException {
		
		os.writeInt(1); // 1 PlanetSquare
		os.writeDouble(getSize());
		os.writeObject(getPosition());
		os.writeDouble(getTimeProduction());
		os.writeUTF(getPath());
		os.writeInt(getCurrentPlayer());
		os.writeDouble(getPower());
		os.writeDouble(getWaveTimer());
		os.writeInt(getUnits());
		
		os.writeInt(getSpaceships().size());
		for(SpaceShip s : getSpaceships()) {
			s.save(os);
		}
	}
	
	

	/**
	 * Override {@link Planet#load(ObjectInputStream, Group, int, int, int, int)}
	 * to load the planet with the square shape
	 */
	public Planet load(ObjectInputStream is, Group root, int player1, int player2, int player3, int player4)
			throws IOException, ClassNotFoundException
	{
		// first integer of inputStream already read by the load function
		
		double size = is.readDouble();
		Point2D position = (Point2D)is.readObject();
		double timeProduction = is.readDouble();
		String path = is.readUTF();
		int currentPlayer = is.readInt();
		double power = is.readDouble();
		double waveTimer = is.readDouble();
		int units = is.readInt();
		
		// creation of the shape of the planet
		Rectangle shape = new Rectangle(position.getX(), position.getY(), size, size);
		
		Planet p = new PlanetSquare(path,size,shape,root,player1,player2,player3,player4);
		// creation of the shape of the planet
		p.setTimeProduction(timeProduction);
		p.setPlayer(currentPlayer);
		p.setPosition(position);
		p.setPower(power);
		p.setWaveTimer(waveTimer);
		p.setUnits(units);
		
		// ships recovery
		SpaceShip s = new SpaceShip();
		ArrayList<SpaceShip> spaceships = new ArrayList<SpaceShip>();
		
		int nbSpaceship = is.readInt();
		for(int i=0; i<nbSpaceship; i++) {
			s = s.load(is, p, root);
			spaceships.add(s);
		}
		
		p.setSpaceships(spaceships);
		
		return p;
		
	}
	
}
