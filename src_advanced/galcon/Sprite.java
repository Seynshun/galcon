package galcon;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.shape.*;

/**
 * Abstract class Sprite used by {@link SpaceShip} and {@link Planet}.
 * <br>
 * It contains a display position (the top-left corner of the image),
 * a size, and a geometric shape for collision calculations.
 */
public abstract class Sprite{
	
	/**
	 * Sprite name
	 */
	protected String name;
	/**
	 * Sprite height
	 */
	private double size;
	/**
	 * Path to the image
	 */
	private String path;
	/**
	 * Displayed image of the sprite
	 */
	private Image image;
	/**
	 * Sprite position
	 */
	private Point2D position;
	/**
	 * Sprite shape for collision management
	 */
	private Shape shape;
	
	/**
	 * 
	 */
	public Sprite() {}
	
	
	
	/**
	 * Create a sprite.
	 * 
	 * @param path Path to the image
	 * @param size Spite height
	 * @param shape Sprite shape
	 */
	public Sprite(String path, double size, Shape shape) {
		image = new Image(path, size, size, false, false);
		this.size = size;
		this.shape = shape;
		this.name = "Sprite";
		this.path = path;
	}
	
	
	
	/**
	 * Create a sprite without associated shape.
	 * @param path Path to the image
	 * @param size Sprite height
	 */
	public Sprite(String path, double size) {
		image = new Image(path, size, size, false, false);
		this.size = size;
		this.shape = null;
		this.name = "Sprite";
		this.path = path;
	}
	
	
	
	/* ===================================== */
	/* ==========GETTER AND SETTER========== */
	/* ===================================== */
	
	

	public Image getImage() {
		return image;
	}

	public String getPath() {
		return path;
	}

	public Shape getShape() {
		return shape;
	}

	public Point2D getPosition() {
		return position;
	}

	public void setPosition(Point2D position) {
		this.position = position.copy();
	}

	public double getSize() {
		return size;
	}

	public void render(GraphicsContext gc) {
		gc.drawImage(image, position.getX(), position.getY());
	}

	public String toString() {
		return name + "<" + position.getX() + ", " + position.getY() + ">";
	}

}
