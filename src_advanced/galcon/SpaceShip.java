package galcon;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javafx.scene.shape.*;
import javafx.scene.Group;

/**
 * The class manages the ships.<br>
 * The ships are linked to a planet.
 * By default they orbit around this planet waiting for a mission.
 * 
 */
public class SpaceShip extends Sprite implements java.io.Serializable{
	/**
	 * ID for serialization
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Ship speed
	 */
	private double speed;
	/**
	 * Current speed of the spaceship, for acceleration
	 */
	private double currentspeed;
	/**
	 * The ship's trajectory when it has a mission
	 * A list of segments traveled one after the other
	 */
	private ArrayList<Segment> trajectory;
	/**
	 * Attack power : damage inflicted on an enemy planet
	 */
	private double power;
	/**
	 * Production time, when a ship arrives on a neutral planet, it initiates a production time
	 */
	private double timeProduction;
	/**
	 * Planet of destination given during the mission
	 */
	private Planet planetdst;
	/**
	 * Source planet that created this ship
	 */
	private Planet planetsrc;
	/**
	 * True if the ship is on the take-off run-way
	 */
	private boolean launched;
	/**
	 * True if the ship has reached its destination
	 */
	private boolean arrived;
	/**
	 * The player controlling the ship
	 */
	private int player;
	/**
	 * The root of the game
	 */
	private Group root;
	/**
	 * Spaceship to switch with different characteristic
	 */
	private SpaceShip switchship;
	/**
	 * If true then the spaceship can't invade other planets
	 */
	private Boolean kamikaze;

	
	
	/**
	 * Empty creator
	 */
	public SpaceShip() {super();}
	
	
	/**
	 * Copy constructor
	 * 
	 * @param s The spaceship to copy
	 */
	public SpaceShip(SpaceShip s) {
		super(s.getPath(),s.getSize());
		this.speed = s.getSpeed();
		this.currentspeed = s.getSpeed();
		this.power = s.getPower();
		this.timeProduction = s.getTimeProduction();
		launched = false;
		arrived = false;
		planetsrc = s.getPlanetsrc();
		this.player = planetsrc.getPlayer();
		trajectory = new ArrayList<Segment>();
		this.root = s.getRoot();
		this.kamikaze = s.getKamikaze();
	}
	


	/**
	 * To create ships:<br>
	 * Call the constructor{@link Sprite}, the ship is only characterized by its central point.
	 * 
	 * @param path Parameter of {@link Sprite}
	 * @param size Parameter of {@link Sprite}
	 * @param speed Ship speed
	 * @param power The attack power
	 * @param timeProduction Production time
	 * @param src The ship's source planet
	 * @param root Root of the game
	 */
	public SpaceShip(String path, double size, double speed, double power, double timeProduction, Planet src, Group root) {
		super(path,size);
		this.speed = speed;
		this.currentspeed = speed;
		this.power = power;
		this.timeProduction = timeProduction;
		launched = false;
		arrived = false;
		planetsrc = src;
		this.player = src.getPlayer();
		trajectory = new ArrayList<Segment>();
		this.root = root;
		kamikaze = false;
	}

	
	
	/**
	 * Updates the position of the ship.<br>
	 * This function is called in {@link Planet#update()}.<br>
	 * 
	 * If the ship has a trajectory it will follow that trajectory.
	 * If he doesn't have one, it's the class {@link Planet}
	 * that's going to be in charge of the ship's position.<br>
	 * 
	 * It is either added to the ship list of the destination planet or in the case of a planet
	 * enemy he inflicts his damage on her.
	 * 
	 */
	public void updatePosition() {
		if(trajectory.size() != 0){
			double acceleration = 0.03;
			
			Segment s = trajectory.get(0);
			// incrementing the point on the first segment of the path list
			Point2D p = new Point2D(super.getPosition().getX() + s.getStep(currentspeed).getX(),
									super.getPosition().getY() + s.getStep(currentspeed).getY());
			// Secondary point indicating the actual point of the ship (upper left corner)
			Point2D realp = new Point2D(p.getX() + super.getSize()/2,p.getY() + super.getSize()/2);
			if(s.isend(realp)) { // If the ship reaches the end of a segment, this segment is deleted and moved to the next segment
				trajectory.remove(0);
				this.setTrajectory(trajectory);
				
				if(trajectory.size() == 0) // spaceship removed if it arrived at the end of its trajectory without reaching its destination
					arrived = true;
			}
			else if(planetdst.isInside(realp)){ // remove last segment when the spaceship arrive at destination
				arrived = true;
				trajectory.clear();
				
				// if the planet is not empty
				if(planetdst.getSpaceships().size() != 0){
					// if the spaceships of the destination is not the same than the player of this ship
					if(planetdst.getSpaceships().get(0).getPlayer() != player) {
						// attack the planet
						planetdst.attack(power);
						
						// If the planet doesn't have any spaceship left it becomes free
						if(planetdst.getSpaceships().size() == 0) {
							planetdst.setSelectedsrc(false);
							planetdst.deleteLine(root);
							planetdst.setPlayer(0);
						}
					}
					else if(!kamikaze){
						// if the planet is owned by the player of the spaceship then we add the spaceship
						planetdst.addSpaceship(this);
					}
					
				}
				else if(!kamikaze){
					// if the planet is free
					planetdst.addSpaceship(this);
				}
			}
			super.setPosition(p);
			currentspeed = currentspeed + acceleration;
		}
	}
	
	
	
	
	/**
	 * Find and update the ship's take-off position according to 
	 * the number of ships already on the take-off run-way , 
	 * of the direction pointed by the mouse and the radius of the planet.
	 *  
	 * @param ind The index of the ship to position it in its location
	 */
	public void setLaunchPosition(double ind) {
		// distance of the radius plus a margin
		double marge = 15;
		Circle c;
		// if the source is a square planet, set the launch position
		// around the circle including the square
		if(planetsrc instanceof PlanetSquare) {
			Rectangle rec = (Rectangle)planetsrc.getShape();
			c = new Circle(rec.getX()+rec.getWidth()/2,rec.getY()+rec.getWidth()/2,rec.getWidth()/Math.sqrt(2));
		}
		else {
			c = (Circle)planetsrc.getShape();
		}
		
		double r = c.getRadius();
		
		double angleMarge = super.getSize() / (r+marge);
		
		double xa = planetsrc.getLine().getStartX();
		double ya = planetsrc.getLine().getStartY();
		
		double xb = planetsrc.getLine().getEndX();
		double yb = planetsrc.getLine().getEndY();
		
		// find the angle of the first ship
		double angle = Math.atan2((yb-ya),(xb-xa));
		
		if(ind % 2 == 0) {
			ind = -ind/2;
		}
		else {
			ind = (int)ind/2;
			
		}
		// change the angle according the the index of the spaceship
		double x = xa + (r+marge)*Math.cos(angle + ind*angleMarge);
		double y = ya + (r+marge)*Math.sin(angle + ind*angleMarge);
		
		setPosition(new Point2D(x -super.getSize()/2,y -super.getSize()/2));
	}
	
	
	
	/**
	 * Secondary function used by {@link #findPath(ArrayList, ArrayList, Planet) findPath}
	 * to find the way-point that allows the ship to dodge a planet. <br>
	 * For this purpose, it finds the point projected on the circle passing through
	 * the line perpendicular to the segment passing through the center of the circle.<br>
	 * This is the highest point to avoid the circle.
	 * a second degree equation is used to find this point.
	 * 
	 * Two points are then found, we choose the one closest to reach
	 * by comparing the distances between these points and the point of
	 * intersection between the segment and the radius of the circle.
	 * 
	 * @param c Circle to avoid
	 * @param s The segment cutting the circle
	 * @return The point of avoidance of the circle
	 */
	public Point2D findWayPoint(Circle c, Segment s) {
		// margin to add to the radius
		double marge = 10;
		
		// for the second degree equation resolution
		double xa = s.getLine().getStartX();
		double ya = s.getLine().getStartY();
		double xb = s.getLine().getEndX();
		double yb = s.getLine().getEndY();
		double xc = c.getCenterX();
		double yc = c.getCenterY();
		
		double invp = (xb-xa)/(yb-ya);
		double p = (yb-ya)/(xb-xa);
		
		double alpha = 1 + Math.pow(invp,2);
		double beta = -2*xc*(1+(Math.pow(invp, 2)));
		double gama = Math.pow(xc, 2)*(1+Math.pow(invp, 2)) - Math.pow((c.getRadius() + marge),2);
		double delta = Math.pow(beta, 2)-4*alpha*gama;
		
		if(delta < 0) { // delta can not be negative
			return null;
		}
		else {
			// way point 1
			double x1 = (-beta + Math.sqrt(delta))/(2*alpha);
			double y1 = invp*(xc-x1)+yc;
			Point2D p1 = new Point2D(x1,y1);
			
			// way point 2
			double x2 = (-beta - Math.sqrt(delta))/(2*alpha);
			double y2 = invp*(xc-x2)+yc;
			Point2D p2 = new Point2D(x2,y2);
			
			// intersect point
			double x = ((yc-ya)+invp*xc+p*xa)/(p+invp);
			double y = p*(x-xa)+ya;
			Point2D intersect = new Point2D(x,y);
			
			// find distances
			double d1 = intersect.distance(p1);
			double d2 = intersect.distance(p2);
			
			// select closer point
			if(d1 <= d2) {
				return p1;
			}
			else {
				return p2;
			}
		}
	}
	
	
	
	
	/**
	 * Replaces a segment with two segments in the segment array. <br>
	 * These two segments are the result of the call to the function{@link Segment#split(Point2D)}.
	 * 
	 * @param tabSegments The segment array
	 * @param seg The segment to be replaced 
	 * @param passage The crossing point that allows the segment to be cut off
	 * @return The segment array passed as a parameter with the requested segment exchanged
	 * by two new segments
	 */
	public ArrayList<Segment> replaceWithSplit(ArrayList<Segment> tabSegments, Segment seg, Point2D passage){
		int index = tabSegments.indexOf(seg);
		
		if(index == -1) return tabSegments;
		
		tabSegments.remove(index);
		ArrayList<Segment> splitSegment = seg.split(passage);
		tabSegments.add(index, splitSegment.get(0));
		tabSegments.add(index+1, splitSegment.get(1));
		return tabSegments;
	}
	
	
	
	
	/**
	 * Finds the list of segments that constitute the path to go from one segment
	 * to another avoiding the planets.<br>
	 * This segment is given in a segment list, because this function is recursive.
	 * The algorithm is looped to find all the related
	 * segments to go from A to B without touching any {@link Planet}.
	 * The Algorithm ends when each segment no longer touches any planet.
	 * 
	 * @param tabPlanets List of Planets to Avoid 
	 * @param tabSegments List of segments forming the path
	 * @param dst Planet destination
	 * 
	 * @return Complete list of segments forming the trajectory avoiding planets
	 * 
	 * @see #findWayPoint(Circle, Segment)
	 * @see #replaceWithSplit(ArrayList, Segment, Point2D)
	 */
	public ArrayList<Segment> findPath(ArrayList<Planet> tabPlanets, ArrayList<Segment> tabSegments, Planet dst) {
		
		ArrayList<Segment> result = new ArrayList<Segment>();
		// Array to copy the result to operate on the original Array
		result.addAll(tabSegments);
		
		for(Segment seg : tabSegments) {
			for(Planet p : tabPlanets) {
				// division of the original segment
				if(p.isCrossed(seg.getLine()) && !(p.equals(dst))) {
					// Find the way point
					Point2D passage;
					// when crossing a square planet, the way point is found
					// using the circle including the square
					if(p instanceof PlanetSquare) {
						Rectangle rec = (Rectangle)p.getShape();
						Circle c = new Circle(rec.getX()+rec.getWidth()/2, rec.getY()+rec.getWidth()/2,rec.getWidth()/Math.sqrt(2));
						
						passage = findWayPoint(c,seg);
					}
					else {
						passage = findWayPoint((Circle)p.getShape(),seg);
					}
					// cut the segment
					result = replaceWithSplit(result, seg, passage);
					// recursively use the result to find the final trajectory
					if(result.size() <= 100) {
						result = findPath(tabPlanets,result,dst);
					}
					else {
						System.out.println("SPACESHIP "+this+" LOST !");
						// if the path is too difficult to find the path is cleared
						// so the ship is destroyed (in Planet#sendUnits)
						result.clear();
					}
				}
			}
		}
		return result;
	}
	
	
	
	/**
	 * Saves a ship.<br>
	 * The settings are saved in the order:
	 * <ul>
	 * <li> 0 if there is no switch ship or 1, the switch ship is then saved </li>
	 * <li> The position </li>
	 * <li> The Size </li>
	 * <li> The speed </li>
	 * <li> The current speed </li>
	 * <li> The power </li>
	 * <li> The production time</li>
	 * <li> Image path to the spaceship image resource</li>
	 * <li> ID of the player controlling the ship </li>
	 * <li> The type of the spaceship (kamikaze or not) </li>
	 * </ul>
	 * And if the spaceship in sent it saves also:
	 * <ul>
	 * <li> The destination planet size </li>
	 * <li> The position of the destination planet </li>
	 * </ul>
	 * 
	 * @param os The saving object
	 * @throws IOException The method potentially contains an exception of type IOException
	 */
	public void save(ObjectOutputStream os) throws IOException {
		if(switchship != null) {
			os.writeInt(1); // the spaceship have a switch ship
			switchship.save(os);
		}
		else {
			os.writeInt(0); // no switch ship
		}
		
		os.writeObject(getPosition());
		os.writeDouble(super.getSize());
		os.writeDouble(speed);
		os.writeDouble(currentspeed);
		os.writeDouble(power);
		os.writeDouble(timeProduction);
		os.writeUTF(super.getPath());
		os.writeInt(player);
		os.writeBoolean(kamikaze);
		
		if(planetdst != null) {
			os.writeObject(planetdst.getPosition());
			os.writeInt(trajectory.size());
			for(Segment s : trajectory) {
				s.save(os);
			}
		}
	}
	
	
	
	/**
	 * load a saved ship.
	 * 
	 * @param is The saving object
	 * @param src Source planet of the ship
	 * @param root Root of the game
	 * 
	 * @return The retrieved ship
	 * 
	 * @throws IOException The method potentially contains an exception of type IOException
	 * @throws ClassNotFoundException The method potentially contains an exception of type ClassNotFoundException
	 */
	public SpaceShip load(ObjectInputStream is, Planet src, Group root)
			throws IOException, ClassNotFoundException
	{
		// is there a switch spaceship to backup ?
		int isSwitch = is.readInt();
		SpaceShip switchship = null;
		if(isSwitch == 1) {
			switchship = new SpaceShip();
			switchship = switchship.load(is, src, root);
		}
		// position not used
		is.readObject();
		double size = is.readDouble();
		double speed = is.readDouble();
		is.readDouble(); // pass current speed
		double power = is.readDouble();
		double timeProduction = is.readDouble();
		String path = is.readUTF();
		int player = is.readInt();
		boolean kamikaze = is.readBoolean();
		
		SpaceShip s = new SpaceShip(path, size, speed, power, timeProduction, src, root);
		s.setPlayer(player);
		s.setSwitchship(switchship);
		s.setKamikaze(kamikaze);
		return s;
	}
	
	
	
	/**
	 * load a saved sent ship.
	 * Here we find the destination of the spaceship comparing the saved
	 * position with all the planets gave as parameters
	 * 
	 * @param is The saving object
	 * @param src Source planet of the ship
	 * @param allPlanets All the planets to find the destination of the spaceship
	 * @param root Root of the game
	 * 
	 * @return The retrieved ship
	 * 
	 * @throws IOException The method potentially contains an exception of type IOException
	 * @throws ClassNotFoundException The method potentially contains an exception of type ClassNotFoundException
	 */
	public SpaceShip load(ObjectInputStream is, Planet src, ArrayList<Planet> allPlanets, Group root)
			throws IOException, ClassNotFoundException
	{
		// is there a switch spaceship to backup ?
		int isSwitch = is.readInt();
		SpaceShip switchship = null;
		if(isSwitch == 1) {
			switchship = new SpaceShip();
			switchship = switchship.load(is, src, root);
		}
		Point2D position = (Point2D)is.readObject();
		double size = is.readDouble();
		double speed = is.readDouble();
		double currentspeed = is.readDouble();
		double power = is.readDouble();
		double timeProduction = is.readDouble();
		String path = is.readUTF();
		int player = is.readInt();
		boolean kamikaze = is.readBoolean();
		
		Point2D center = (Point2D)is.readObject();
		
		int nbSegments = is.readInt();
		
		// create the spaceship
		SpaceShip s = new SpaceShip(path, size, speed, power, timeProduction, src, root);
		s.setPosition(position);
		s.setCurrentspeed(currentspeed);
		s.setPlayer(player);
		
		// find its destination
		for(Planet p : allPlanets) {
			if(p.getPosition().equals(center)) {
				s.setPlanetdst(p);
				break;
			}
		}
		
		// backup trajectory of spaceship
		ArrayList<Segment> tabseg = new ArrayList<Segment>();
		Segment seg = new Segment();
		for(int i=0; i<nbSegments; i++) {
			tabseg.add(seg.load(is));
		}
		
		s.setTrajectory(tabseg);
		s.setSwitchship(switchship);
		s.setKamikaze(kamikaze);
		return s;
	}
	
	
	
	/* ===================================== */
	/* ==========GETTER AND SETTER========== */
	/* ===================================== */
	
	

	public Planet getPlanetdst() {
		return planetdst;
	}

	public void setPlanetdst(Planet planetdst) {
		this.planetdst = planetdst;
	}

	public ArrayList<Segment> getTrajectory() {
		return trajectory;
	}
	
	
	public void setTrajectory(ArrayList<Segment> trajectory) {
		this.trajectory = trajectory;
		launched = true;
	}

	public boolean isLaunched() {
		return launched;
	}

	public void setLaunched(boolean launched) {
		this.launched = launched;
	}

	public boolean isArrived() {
		return arrived;
	}

	public double getSpeed() {
		return speed;
	}

	public double getPower() {
		return power;
	}

	public void setPower(double power) {
		this.power = power;
	}

	public int getPlayer() {
		return player;
	}

	public void setPlayer(int player) {
		this.player = player;
	}

	public double getTimeProduction() {
		return timeProduction;
	}

	public void setCurrentspeed(double currentspeed) {
		this.currentspeed = currentspeed;
	}

	public void setTimeProduction(double timeProduction) {
		this.timeProduction = timeProduction;
	}

	public SpaceShip getSwitchship() {
		return switchship;
	}

	public void setSwitchship(SpaceShip switchship) {
		this.switchship = switchship;
	}

	public Planet getPlanetsrc() {
		return planetsrc;
	}

	public Group getRoot() {
		return root;
	}
	
	/**
	 * set player owning the ship when setting a source planet
	 * 
	 * @param planetsrc The planet source to set
	 */
	public void setPlanetsrc(Planet planetsrc) {
		this.planetsrc = planetsrc;
		this.player = planetsrc.getCurrentPlayer();
	}

	public Boolean getKamikaze() {
		return kamikaze;
	}

	public void setKamikaze(Boolean kamikaze) {
		this.kamikaze = kamikaze;
	}
	
}