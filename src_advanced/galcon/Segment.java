package galcon;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javafx.scene.shape.*;

/**
 * Create a segment that has a direction vector.<br>
 * 
 * Allows to perform geometric operations on the segment :
 * <ul>
 * <li>Know if a point has crossed the end of the segment</li>
 * <li>Cut a segment in half through an intermediate point</li>
 * <li>Get the vector of a segment</li>
 * <li>
 * Increase the point on the segment according to a velocity
 * and the vector of this line
 * </li>
 * </ul>
 *
 */
public class Segment implements java.io.Serializable{
	
	/**
	 * ID for serialisation
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * line
	 */
	private Line line;
	/**
	 * Associated direction vector
	 */
	private Point2D vect;
	/**
	 * Name
	 */
	private String name;
	
	
	
	/**
	 * Empty creator
	 */
	public Segment() {	}
	
	
	
	
	
	/**
	 * Create a segment from a line
	 * @param line The line
	 */
	public Segment(Line line) {
		this.line = new Line(line.getStartX(),line.getStartY(),line.getEndX(),line.getEndY());
		// find the associated direction vector
		this.vect = findVect(line);
		name = "Segment";
	}
	
	
	
	
	/**
	 * Determine if a point has reached the end of the line
	 * depending on the direction vector and the coordinates of the end point of the line.
	 * @param position The point to check
	 * @return True if the point has passed the end, else false
	 */
	public boolean isend(Point2D position) {
		double endx = line.getEndX();
		double endy = line.getEndY();
		double vectx = vect.getX();
		double vecty = vect.getY();
		double px = position.getX();
		double py = position.getY();
		
		if(vectx < 0) {
			if(px <= endx) return true;
			else return false;
		}
		else if(vectx > 0){
			if(px >= endx) return true;
			else return false;
		}
		else {
			if(vecty < 0) {
				if(py <= endy) return true;
				else return false;
			}
			else if(vecty > 0){
				if(py >= endy) return true;
				else return false;
			}
			else return false;
		}
	}
	
	
	
	
	/**
	 * Cuts a segment in half through a point.
	 * 
	 * @param p The point of the cut
	 * @return An array with the two new segments created
	 */
	public ArrayList<Segment> split(Point2D p){
		ArrayList<Segment> segmentsplit = new ArrayList<Segment>();
		double x1 = line.getStartX();
		double y1 = line.getStartY();
		double x2 = line.getEndX();
		double y2 = line.getEndY();
		Line l1 = new Line(x1,y1,p.getX(),p.getY());
		Line l2 = new Line(p.getX(),p.getY(),x2,y2);
		
		Segment s1 = new Segment(l1);
		Segment s2 = new Segment(l2);
		
		segmentsplit.add(s1);
		segmentsplit.add(s2);
		
		return segmentsplit;
	}
	
	/**
	 * Find the direction vector of the line.
	 * @param line The line for which the direction vector must be determined
	 * @return A {@link Point2D} with the coordinates of the direction vector
	 */
	public Point2D findVect(Line line) {
		double xa = line.getStartX();
		double ya = line.getStartY();
		double xb = line.getEndX();
		double yb = line.getEndY();
		
		double a = xb-xa;
		double b = yb-ya;
		
		return new Point2D(a,b);
	}
	
	/**
	 * Advance a point on the segment.
	 * you need to increment the coordinates of the point
	 * with the values returned by this function.
	 * 
	 * @param speed The desired speed of the displacement
	 * @return A {@link Point2D} with the increment coordinates
	 */
	public Point2D getStep(double speed) {
		
		double vx = vect.getX();
		double vy = vect.getY();
		
		double d = Math.sqrt(vx*vx + vy*vy);
		
		double x = speed*(vx/d);
		double y = speed*(vy/d);
				
		return new Point2D(x,y);
	}
	
	
	/**
	 * Save the coordinates of the line of the segment
	 * in the backup object
	 *  
	 * @param os backup object
	 * @throws IOException The method potentially contains an exception of type IOException
	 */
	public void save(ObjectOutputStream os) throws IOException {
		os.writeDouble(line.getStartX());
		os.writeDouble(line.getStartY());
		os.writeDouble(line.getEndX());
		os.writeDouble(line.getEndY());
	}
	
	
	
	/**
	 * Load back the segment saved from the
	 * backup object
	 * 
	 * @param is the backup object
	 * @return the segment loaded
	 * @throws IOException The method potentially contains an exception of type IOException
	 */
	public Segment load(ObjectInputStream is) throws IOException {
		double startX = is.readDouble();
		double startY = is.readDouble();
		double endX = is.readDouble();
		double endY = is.readDouble();
		Line line = new Line(startX,startY,endX,endY);
		
		return new Segment(line);
	}
	/* ===================================== */
	/* ==========GETTER AND SETTER========== */
	/* ===================================== */
	
	
	
	public Line getLine() {
		return line;
	}

	public String toString() {
		return name + "<line:" + line + ", vecteur:" + vect + ">";
	}
	
}
