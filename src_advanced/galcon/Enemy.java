package galcon;

import java.util.ArrayList;
import java.util.Random;

import javafx.scene.Group;

/**
 * 
 * Autonomous player.
 * Depending on the situation, he selects a planet of his own and
 * sends a random number of troops on mission.
 * If the enemy's power is greater than the power
 * of the opposing player, he will tend to attack more often.
 * If it is weaker it will focus on invading free planets if possible.
 * When a planet is left with too few ships it will send troops to
 * rescue the planet.
 * 
 */
public class Enemy {
	
	/**
	 * Root of the game
	 */
	private Group root;
	/**
	 * Computer player ID
	 */
	private int id;
	/**
	 * All the planets of the game
	 */
	private ArrayList<Planet> allPlanets;
	/**
	 * Progress of actions
	 */
	private double currentTime;
	/**
	 * Delay between each actions
	 */
	private double actionDelay; 
	/**
	 * Planet selected as mission target
	 */
	private Planet currentDst;
	/**
	 * Planet of the computer selected as the source for a mission
	 */
	private Planet currentSrc;
	
	
	
	/**
	 * Create an enemy computer.
	 * 
	 * @param allPlanets Main planets
	 * @param actionDelay Delay between each action
	 * @param root Root of the game
	 * @param id Computer player number
	 */
	public Enemy(ArrayList<Planet> allPlanets, double actionDelay, Group root, int id){
		this.allPlanets = allPlanets;
		this.actionDelay = actionDelay;
		this.root = root;
		currentDst = null;
		currentSrc = null;
		this.id = id;
	}
	
	
	
	/**
	 * Find a theoretical random number of units to send.
	 * For this purpose the function looks at the average of the owned ship.
	 * This function is used by {@link #doSomething()}
	 * which if too few ships are possessed, cancels a mission.
	 * 
	 * @return Theoretical number of ships to be sent
	 */
	public int nbUnits() {
		
		double planets = 0;
		double units = 0;
		
		for(Planet p : allPlanets) {
			if(p.getPlayer() == id) {
				units += p.getSpaceships().size();
				planets++;
			}
		}
		
		double mediumUnits = units/planets;
		if(mediumUnits == 0) {
			return 0;
		}
		else {
			Random rand = new Random();
			int val = rand.nextInt((int)(mediumUnits)) + 1;
			if(val > 50) {
				return 50;
			}
			else {
				return val;
			}
		}
	}
	
	
	
	/**
	 * Find a theoretical random number of units to send.
	 * For this purpose the function looks at the average of the owned ship.
	 * This function is used by {@link #doSomething()} which
	 * if too few ships are possessed, cancels a mission.
	 * 
	 * @param div Number of division
	 * @param p The planet
	 * 
	 * @return Theoretical number of ships to be sent
	 */
	public int nbUnits(Planet p, int div) {
		
		double units = 0;
	
		if(p.getPlayer() == id) {
			units += p.getSpaceships().size();
		}
		
		return (int) (units/div + Math.random()*((div-1)*units/div));
	}
	
	
	
	/**
	 * Select a planet randomly.
	 * 
	 * @param tab The table of planets from which one is chosen
	 * 
	 * @return A planet
	 */
	public Planet selectRandPlanet(ArrayList<Planet> tab) {
		
		Random rand = new Random();
		int pos = rand.nextInt(tab.size());
		
		return tab.get(pos);
	}
	
	
	
	/**
	 * To know the number of planets the target has.
	 * @return The number of planets owned by the target
	 */
	public double allTargetPower() {
		int nbEnemies = 3;
		double power = 0;
		
		for(Planet p : allPlanets) {
			if(p.getPlayer() != 0 && p.getPlayer() != id) {
				for(SpaceShip s : p.getSpaceships()) {
					power += s.getPower();
				}
			}
		}
		return power/nbEnemies;
	}
	
	
	
	/**
	 * To know the number of planets the computer has.
	 * @return The number of planets owned by the computer
	 */
	public double allEnemyPower() {
		double power = 0;
		
		for(Planet p : allPlanets) {
			if(p.getPlayer() == id) {
				for(SpaceShip s : p.getSpaceships()) {
					power += s.getPower();
				}
			}
		}
		return power;
	}

	
	/**
	 * To know the ratio between the power of the target and the computer.
	 * @return The allEnemyPower / allTargetPower ratio
	 */
	public double powerRatio() {
		return allEnemyPower() / allTargetPower();
	}
	
	
	
	/**
	 * Indicates if the planet passed in parameter is in critical state.
	 * A planet is in critical condition if it is in need of a ship
	 * compared to the other planets on the computer.
	 * @see #findCritical()
	 * @param p The planet
	 * @return True if in critical condition, else false
	 */
	public boolean isCritical(Planet p) {
		double criticalRatio = 0.25;
		
		double power = 0;
		for(SpaceShip s : p.getSpaceships()) {
			power += s.getPower();
		}
		
		return power < (allEnemyPower()/getEnemy().size())*criticalRatio;
	}
	
	
	
	/**
	 * Find or not a critical planet.
	 * 
	 * @see #isCritical(Planet)
	 * @return The critical planet found or null if there is not one
	 */
	public Planet findCritical() {
		for(Planet p : getEnemy()) {
			if(isCritical(p)) {
				return p;
			}
		}
		return null; 
	}
	
	
	
	/**
	 * Adds free planets to an array.
	 * @return The array of free planets
	 */
	public ArrayList<Planet> getFree(){
		ArrayList<Planet> free = new ArrayList<Planet>();
		for(Planet p : allPlanets) {
			if(p.getPlayer() == 0) {
				free.add(p);
			}
		}
		
		return free;
	}
	
	
	
	/**
	 * Adds the target's planets to an array.
	 * @return The array with the target's planets
	 */
	public ArrayList<Planet> getTarget(){
		ArrayList<Planet> player = new ArrayList<Planet>();
		for(Planet p : allPlanets) {
			if(p.getPlayer() != 0 && p.getPlayer() != id) {
				player.add(p);
			}
		}
		
		return player;
	}
	
	
	
	/**
	 * Adds computer planets to an array.
	 * @return The computer's array of planets
	 */
	public ArrayList<Planet> getEnemy(){
		ArrayList<Planet> enemy = new ArrayList<Planet>();
		for(Planet p : allPlanets) {
			if(p.getPlayer() == id) {
				enemy.add(p);
			}
		}
		
		return enemy;
	}
	
	
	
	/**
	 * Determines whether or not an action should be cancelled.
	 * @see #nbUnits()
	 * @return True if too few ships are owned
	 */
	public boolean doSomething() {
		if(nbUnits() < 3) {
			return false;
		}
		else {
			return true;
		}
	}
	
	
	
	/**
	 * Computer update function.
	 * At the end of each timer {@link #actionDelay} an action is launched.
	 * The first step is to choose the destination planet.
	 * if a destination planet has been found then a source planet is chosen.<p>
	 * 3 actions are then executed one after the other with a short delay
	 * between them. First the selection line is displayed,
	 * then units are placed on the take-off track, finally, they are sent.
	 */
	public void update() {
		double minRatioToAttack = 1.7; // minimum ratio to decide to attack
		double decision = 0.95; // Chances to take the right decision
		int division = 5; // How much to divided the numbers of ships for the minimum ships to send
		
		// increment computer time
		currentTime++;
		if(currentTime >= actionDelay) {
			currentTime = 0;
		}
		
		// select a critical planet or not
		// if power ratio is low (protection mode)
		if(currentDst == null && powerRatio() <= 0.95) {
			currentDst = findCritical();
		}
		
		// choose the destination
		if(currentDst == null) {
			if(getFree().size() == 0) { // If no free planets is left then attack
				ArrayList<Planet> tab = getTarget();
				if(tab.size() != 0) {
					currentDst = selectRandPlanet(tab);
				}
			}
			else if(getTarget().size() != 0) {
				// if power ratio is enough then attack, in case of a good decision
				if(powerRatio() >= minRatioToAttack) {
					if(Math.random() <= decision) { // good choice
						currentDst = selectRandPlanet(getTarget());
					}
					else { // bad choice
						currentDst = selectRandPlanet(getFree());
					}
				}
				else{ // else we look to invade free planets
					if(Math.random() <= decision) { // good choice
						currentDst = selectRandPlanet(getFree());
					}
					else { // bad choice
						currentDst = selectRandPlanet(getTarget());
					}
				}
			}
			if(!doSomething()) currentDst = null;
		}
		else{ // mission launched if we selected a destination
			
			// choose a source
			if(currentSrc == null || currentSrc.getPlayer() != id) {
				currentSrc = selectRandPlanet(getEnemy());
				
				// BUG FIXED !
				// if we can't find another planet to send spaceship to the critical one,
				// then we choose a target as destination
				int loop = 0;
				while(currentDst == currentSrc) {
					if(loop > 100) {
						// new target
						currentDst = selectRandPlanet(getTarget());
						break;
					}
					currentSrc = selectRandPlanet(getEnemy());
					loop++;
				}
			}
			// first action, draw the line
			if(currentTime == 0) {
				currentSrc.drawLine(currentDst, root);
			}
			// second, hire units
			if(currentTime == 10) {
				double units = nbUnits(currentSrc,division);
				for(int i=0; i<units; i++) {
					currentSrc.hireUnit();
				}
			}
			// send units on the mission
			if(currentTime == 30) {
				if(currentSrc.getLine() != null) {
					currentSrc.giveMission(currentDst, allPlanets);
					currentSrc.deleteLine(root);
					currentSrc = null; // pour la selection d'une autre source
					currentDst = null; // pour la selection d'une autre destination
				}
			}
			
		}
	}

	
	
	/* ===================================== */
	/* ==========GETTER AND SETTER========== */
	/* ===================================== */
	
	

	public void setAllPlanets(ArrayList<Planet> allPlanets) {
		this.allPlanets = allPlanets;
	}
	
}
