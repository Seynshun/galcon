package galcon;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javafx.scene.shape.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.input.MouseEvent;

/**
 * The planet class is in charge of the management of the planet's star-ships.
 * <ul>
 * <li>
 * The waiting ships are displayed rotating around the planet,
 * they are stored in a array.
 * </li>
 * <li>
 * When a mission is selected (click/drop on a planet) one ship is proposed for sending.
 * </li>
 * <li>
 * The player can increase or decrease the number of ships (at zero the mission is cancelled).
 * </li>
 * <li>
 * After validation of the mission, the selected ships are placed in another array where they can no longer be selected for another mission and will follow their trajectory until they reach their destination.
 * </li>
 * </ul>
 */
public class Planet extends Sprite implements java.io.Serializable {
	/**
	 * ID for serialization
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Production time of a ship
	 */
	private double timeProduction;
	
	/**
	 * Production progress
	 */
	private double currentTime;
	
	/**
	 * Timer to send wave of spaceships
	 */
	private double waveTimer;
	
	/**
	 * Planet's owner : 0: anyone, 1: player, 2: computer
	 */
	private int currentPlayer;
	
	/**
	 * Power of the new spaceships created on the planet
	 */
	private double power;
	
	/**
	 * List of ships waiting to be sent
	 */
	private ArrayList<SpaceShip> spaceships;
	
	/**
	 * List of sent ships
	 */
	private ArrayList<SpaceShip> shipSent;
	
	/**
	 * Indicates if the planet is selected as a source by the player
	 */
	private boolean selectedsrc;
	
	/**
	 * Indicates if the planet is selected as a destination by the player
	 */
	private boolean selecteddst;
	
	/**
	 * Line connecting the source planet to the destination planet
	 * is used for esthetics and also for the necessary geometric calculation.
	 */
	private Line line;
	
	/**
	 * Angle giving the current rotation of the ships in orbit
	 */
	private double currentAngle;
	
	/**
	 * The number of units selected for a future mission, 
	 * after each troop sending it is reset to zero.
	 */
	private int units;
	
	/**
	 * Root of the game for the display
	 */
	private Group root;
	
	/**
	 * Player 1 , his planets are cyan
	 */
	private int player1;
	
	/**
	 * Player 2 , his planets are red
	 */
	private int player2;
	
	/**Third player*/
	private int player3;
	
	/**Fourth player*/
	private int player4;
	
	/**
	 * Text showing the number of spaceships
	 */
	private Label label;
	
	/**
	 * All the planets of the game to find the path to send the spaceship wave by wave
	 */
	private ArrayList<Planet> allPlanets;
	
	
	/**
	 *  Builder without initialization for loading saved planet.
	 */
	public Planet() {super();}
	
	
	
	
	/**
	 * Build a planet.
	 * 
	 * @param path Path for the planet image
	 * @param size The size of the planet
	 * @param shape The shape of the planet for geometric calculation
	 * @param root Root of the game
	 * @param player1 Player with cyan planet
	 * @param player2 Player with red planet
	 * @param player3 Player with white planet
	 * @param player4 Player with orange planet
	 */
	public Planet(String path, double size, Shape shape, Group root, int player1, int player2, int player3, int player4) {
		super(path,size,shape);
		name = "Planet";
		this.timeProduction = 0;
		spaceships = new ArrayList<SpaceShip>();
		shipSent = new ArrayList<SpaceShip>();
		selectedsrc = false;
		selecteddst = false;
		this.root = root;
		currentAngle = 0;
		currentTime = 0;
		units = 0;
		allPlanets = null;
		this.player1 = player1;
		this.player2 = player2;
		this.player3 = player3;
		this.player4 = player4;
	}
	
	
	
	/**
	 * Give to all the spaceships the destination planet so they can
	 * go on mission. This method call a first time {@link #sendWave()}
	 * to initiate the launching.
	 * 
	 * @param dst the destination planet to send the spaceship on
	 * @param tabPlanets all the planet of the game to find the path through them
	 */
	public void giveMission(Planet dst, ArrayList<Planet> tabPlanets) {
		allPlanets = tabPlanets;
		
		for(int i=0; i<units; i++) {
			SpaceShip s = spaceships.get(i);
			s.setPower(power);
			s.setPlanetdst(dst); // initialization of the destination planet
		}
		// send first wave
		sendWave();
	}
	
	
	
	/**
	 * Send a wave of spaceships to the destination.
	 * It finds the number of spaceships to send and then give them
	 * the trajectory so they can go.
	 * When the waveTimer is equal to 1 in starts to increment so the next
	 * waves can go after the first one.
	 * It is set to zero when there is no more units
	 */
	public void sendWave() {
		waveTimer = 1;
		
		double shipsize = spaceships.get(0).getSize();
		
		// find the number of spaceship to send
		int nbToSend;
		// send a complete wave it there is too many ships to send
		if(units*shipsize >= Math.PI*(getSize()+30)) {
			nbToSend = (int)(Math.PI*(getSize()+30)/shipsize);
			units = units - nbToSend;
		}
		// send all spaceships if it is possible
		else {
			nbToSend = units;
			units = 0;
		}
		
		// send the wave of spaceships
		for(int i=0; i<nbToSend; i++) {
			
			SpaceShip s = spaceships.get(i);
			
			if(s.getPlanetdst() != null) {
				// take back the destination of the spaceship
				Planet dst = s.getPlanetdst();
				
				ArrayList<Segment> tabseg = new ArrayList<>();
				Line l;
				// if the destination is a square : use end point as the center of the square
				if(dst instanceof PlanetSquare) {
					Rectangle rec = (Rectangle)dst.getShape();
					l= new Line(s.getPosition().getX() + s.getSize()/2, s.getPosition().getY() + s.getSize()/2,
							 rec.getX()+rec.getWidth()/2, rec.getY()+rec.getWidth()/2);
				}
				else {
					Circle c = (Circle)dst.getShape();
					// original path before cutting
					l= new Line(s.getPosition().getX() + s.getSize()/2, s.getPosition().getY() + s.getSize()/2,
									 c.getCenterX(), c.getCenterY());
				}
				
				tabseg.add(new Segment(l));
				// cutting the trajectory
				s.setTrajectory(s.findPath(allPlanets, tabseg, dst));
				// the ship is sent only if it founds a trajectory, otherwise it is destroyed
				if(s.getTrajectory().size() != 0) {
					shipSent.add(s); // adding ships to the sent ships array
				}
			}
		}
		
		removeAllSentShip(nbToSend);
		
		if(units == 0) {
			waveTimer = 0;
		}
	}
	
	
	/**
	 * Add a ship to the mission.
	 * 
	 * The number of Units is incremented 
	 * and a new ship is added as launched.
	 */
	public void hireUnit() {
		if(spaceships.size() > 0 && units < spaceships.size() -1 && line != null) {
			spaceships.get(units).setLaunched(true);
			units++;
		}
	}
	
	
	
	
	/**
	 * Removal of a ship from the mission,
	 * the number of Units is decremented.
	 * 
	 */
	public void dismissUnit() {
		if(units > 0) {
			units--;
			spaceships.get(units).setLaunched(false);
		}
	}
	
	
	
	
	/**
	 * Remove all ships from the take-off run-way.
	 * 
	 */
	public void dismissAllUnits() {
		for(int i=0; i<units; i++) {
			spaceships.get(i).setLaunched(false);
		}
		units = 0;
	}
	
	
	
	
	/**
	 * Remove the sent ships from the shipboard.
	 * @param n Number of ships to remove
	 */
	public void removeAllSentShip(int n) {
		for(int i=0; i<n; i++) {
			SpaceShip s = spaceships.get(0);
			spaceships.remove(s);
		}
	}
	
	
	
	
	/**
	 * Adding a ship to the planet.
	 * It gives to the planet his production time, player and power
	 * for the planet to reproduce spaceships
	 * 
	 * @param s Add the ship to the ship list of the planet
	 */
	public void addSpaceship(SpaceShip s) {
		
		// set parameters
		currentPlayer = s.getPlayer();
		timeProduction = s.getTimeProduction();
		power = s.getPower();
		
		// create new spaceship from the one arrived
		SpaceShip newship = new SpaceShip(s);
		newship.setPlanetsrc(this);
		
		if(s.getSwitchship() != null) {
			// create and add the switch ship from the ship arrived
			SpaceShip shipswitch = new SpaceShip(s.getSwitchship());
			shipswitch.setPlanetsrc(this);
			newship.setSwitchship(shipswitch);
		}
		
		spaceships.add(newship);
	}
	
	
	
	
	/**
	 * Adding a ship to the planet.<p>
	 * The added ship will take the characteristics of the first ship in the table.
	 * If no ships are in the array, the function does not add a ship.
	 */
	public void addSpaceship() {
		if(spaceships.size() != 0) {
			
			// create new ship from the origin one
			SpaceShip origin = spaceships.get(0);
			SpaceShip newship = new SpaceShip(origin);
			
			if(origin.getSwitchship() != null) {
				// create and add the switch ship
				SpaceShip shipswitch = new SpaceShip(origin.getSwitchship());
				newship.setSwitchship(shipswitch);
			}
			
			// restore original power if the ship have been attacked
			newship.setPower(power);
			
			spaceships.add(newship);
		}
	}
	
	
	
	
	/**
	 * Attack on the planet when an enemy ship arrives on it.<br>
	 * The damage is inflicted on the planet's ships, subtracting the power
	 * of the arriving ship from the power of the planet's ships.
	 * 
	 * @param power The damage inflicted 
	 */
	public void attack(double power) {
		if(spaceships.size() != 0) {
			
			ArrayList<SpaceShip> copyShip = new ArrayList<SpaceShip>();
			copyShip.addAll(spaceships);
			// damage is dealt to all ships as long as attack power remains
			for(int i=copyShip.size()-1; i>=0; i--) {
				SpaceShip s = copyShip.get(i);
				
				double defence = s.getPower();
				s.setPower(s.getPower() - power); // damage dealt
				power -= defence; // power left
				
				if(s.getPower() <= 0){
					// check out units is strictly lower than the nb of spaceships
					if(units == spaceships.size()) {
						dismissUnit();
					}
					spaceships.remove(s);
				}
				// when all damages have been dealt
				if(power <= 0) {
					break;
				}
			}
		}
	}
	
	
	
	
	/**
	 * Ship movement.<br>
	 * 
	 * <ul>
	 * <li>
	 * Depending on whether the ship is a standby, in orbit, or on mission,
	 * he has to move it differently.
	 * </li>
	 * <li>
	 * A ship in orbit must rotate around the planet, its position is determined by its order in the spaceships array.
	 * </li>
	 * <li>
	 * A ship on the run-way is assigned a position with the function
	 * {@link SpaceShip#setLaunchPosition(double)}.
	 * </li>
	 * <li>
	 * A moving ship uses the function {@link SpaceShip#updatePosition()}.
	 * When a ship has arrived at its destination it is removed from the array.
	 * </li>
	 * </ul>
	 */
	public void moveSpaceships() {
		double marge = 9; // distance spaceship - planet
		double rotateSpeed = Math.PI/500; // rotation speed 
		
		currentAngle = (currentAngle + rotateSpeed) % (2*Math.PI); // increment the angle
		
		Circle c = (Circle)super.getShape();
		double cx = c.getCenterX();
		double cy = c.getCenterY();
		double r = c.getRadius();
		
		for(SpaceShip s : spaceships) {
			// spaceship rotation
			if(!s.isLaunched() && s.getTrajectory().size()==0) {

				double angle = s.getSize() / (r+marge);
						
				int ind = spaceships.indexOf(s);
				
				double x = cx + (r+marge)*Math.cos(currentAngle - ind*angle);
				double y = cy + (r+marge)*Math.sin(currentAngle - ind*angle);

				Point2D position = new Point2D(x-s.getSize()/2,y-s.getSize()/2);

				s.setPosition(position);
			}
			// position of a spaceship launched
			else if(s.isLaunched() && line != null) {
				int ind = spaceships.indexOf(s)+1;
				s.setLaunchPosition(ind);
			// position of a spaceship sent determined in 
			}
		}
		
		ArrayList<SpaceShip> copyShipSent = new ArrayList<SpaceShip>();
		copyShipSent.addAll(shipSent);
		for(SpaceShip s : copyShipSent) {
			s.updatePosition();
			if(s.isArrived()) {
				shipSent.remove(s); // remove the spaceship when arrived
			}
		}
	}
	
	
	
	
	/**
	 * Updates the planet.<br>
	 * 
	 * Moving ships with the function {@link #moveSpaceships()}.
	 * The progression time is incremented and a new ship is added to the target time.<p>
	 * maxProd indicates the maximum number of ships built per planet.
	 * 
	 * @see #addSpaceship()
	 * @see #moveSpaceships()
	 */
	public void update() {
		// maximum production
		double maxProd = 500;

		moveSpaceships();
		
		if(waveTimer != 0) {
			waveTimer++;
		}
		// send the spaceships wave by wave
		if(units > 0 && waveTimer%40==0 && spaceships.get(0).getPlanetdst() != null) {
			sendWave();
		}
		
		currentTime ++;
		if(currentTime >= timeProduction && timeProduction != 0) {
			currentTime = 0;
			if(spaceships.size() < maxProd)
			addSpaceship();
		}
	}
	
	
	
	
	/**
	 * Create the line between this planet and the one passed as an argument.
	 * 
	 * @param p2 The destination planet
	 * @param root The root of the game to remove the old line
	 */
	public void drawLine(Planet p2, Group root) {
		root.getChildren().remove(line);
		Circle c = (Circle)super.getShape();
		// use center of the square if it is a square planet
		if(p2 instanceof PlanetSquare) {
			Rectangle rec = (Rectangle)p2.getShape();
			line = new Line(c.getCenterX(), c.getCenterY(),
					rec.getX()+rec.getWidth()/2, rec.getY()+rec.getWidth()/2);
		}
		else {
			Circle c2 = (Circle)p2.getShape();
			line = new Line(c.getCenterX(), c.getCenterY(), c2.getCenterX(), c2.getCenterY());
		}
	}
	
	
	
	/**
	 * Draws the line to where the mouse is pointing.
	 * 
	 * @param e The mouse event object
	 * @param root The root of the game
	 */
	public void drawLine(MouseEvent e, Group root) {
		root.getChildren().remove(line);
		Circle c = (Circle)super.getShape();
		line = new Line(c.getCenterX(), c.getCenterY(), e.getX(), e.getY());
	}
	
	
	
	/**
	 * Function that removes the line from the planet by deleting it.
	 * 
	 * @param root The root of the game
	 */
	public void deleteLine(Group root) {
		root.getChildren().remove(line);
		line = null;
	}
	
	
	
	/**
	 * Create text with number of spaceships.
	 * When there's too much unit to send the number of units is printed
	 * @param root The root of the game
	 */
	public void drawText(Group root) {
		String nb;
		root.getChildren().remove(label);
		label = new Label();
		
		int total = spaceships.size();
		double shipsize = spaceships.get(0).getSize();
		
		// if units is too big we print it
		if(units*shipsize >= Math.PI*(getSize()+30)) {
			nb = String.valueOf(units);
			label.setTextFill(Color.RED);
		}
		// else we print the total of ships minus units
		else if((total-units)*shipsize >= Math.PI*(getSize()+18)){
			nb = String.valueOf(total-units);
			label.setTextFill(Color.DARKBLUE);
		}
		else {
			nb = "";
		}
			
		label.setText(nb);
		label.setFont(Font.font("Helvetica", FontWeight.BOLD, 20));
		
		label.setPrefSize(100, 50);
		label.setAlignment(Pos.CENTER);
		label.setTranslateX(getPosition().getX() + getSize()/2 - label.getPrefWidth()/2);
		label.setTranslateY(getPosition().getY() + getSize()/2 - label.getPrefHeight()/2);
		
		root.getChildren().add(label);
		
	}
	
	
	/**
	 * remove the label from the root
	 * @param root The root of the game
	 */
	public void removeLabel(Group root) {
		root.getChildren().remove(label);
	}
	
	
	
	
	/**
	 * Planet rendering function.<br>
	 * It calls the rendering functions of the ships of the 2 arrays {@link #spaceships} {@link #shipSent}.
	 * If a line is registered it will be displayed in the corresponding color according to the player possessing the planet.
	 * The circles of the planets are of different colors, depending on whether they are selected as a source,
	 * as a destination, or by default depending on whether they are owned by a player or not.
	 * 
	 * @param gc The graphic context to display the images
	 * @param root The root of the game to display the shapes
	 */
	public void render(GraphicsContext gc, Group root) {
		// render all spaceships
		for(SpaceShip s : spaceships) {
			if(s.getPosition() != null) {
				
				// units ready for mission
				if(spaceships.indexOf(s) < units &&
				   (spaceships.indexOf(s)+1)*s.getSize() <= Math.PI*(getSize() + 30))
				{
					s.render(gc);
				}
				// spaceship in orbital
				else if((spaceships.indexOf(s) >= units && 
						 (spaceships.indexOf(s)-units+1)*s.getSize() <= Math.PI*(getSize() + 18)))
				{
					s.render(gc);
				}
			}
		}
		
		// draw number of spaceships if needed
		if(spaceships.size() != 0) {
			drawText(root);
		}
		
		for(SpaceShip s : shipSent) {
			if(s.getPosition() != null) {
				s.render(gc);
			}
		}
		
		// render picture of the planet
		gc.drawImage(super.getImage(), super.getPosition().getX(), super.getPosition().getY());
		Circle c = (Circle)super.getShape(); // creating shape of the planet
		c.setStrokeWidth(2);
		c.setFill(null);
		
		if(line != null) {
			root.getChildren().remove(line);
			// show lines
			if(currentPlayer == player1) {
				line.setStroke(Color.MAGENTA);
			}
			else if(currentPlayer == player2){
				line.setStroke(Color.RED);
			}
			else if(currentPlayer == player3) {
				line.setStroke(Color.WHITESMOKE);
			}
			else if(currentPlayer == player4) {
				line.setStroke(Color.ORANGE);
			}
			root.getChildren().add(line);
		}
		
		if(selectedsrc) { // color for source
			c.setStroke(Color.YELLOW);
			root.getChildren().remove(c);
			root.getChildren().add(super.getShape());
		}
		else if(selecteddst) { // color for destination
			c.setStroke(Color.LAWNGREEN);
			root.getChildren().remove(c);
			root.getChildren().add(super.getShape());
		}
		else {
			if(this.currentPlayer == player1) { // color joueur1
				c.setStroke(Color.MAGENTA);
				root.getChildren().remove(c);
				root.getChildren().add(super.getShape());
			}
			else if(this.currentPlayer == player2) { // color joueur2
				c.setStroke(Color.RED);
				root.getChildren().remove(c);
				root.getChildren().add(super.getShape());
			}
			else if(this.currentPlayer == player3) { // color joueur3
				c.setStroke(Color.WHITESMOKE);
				root.getChildren().remove(c);
				root.getChildren().add(super.getShape());
			}
			else if(this.currentPlayer == player4) { // color joueur4
				c.setStroke(Color.ORANGE);
				root.getChildren().remove(c);
				root.getChildren().add(super.getShape());
			}
			else {
				c.setStroke(Color.BLUE); // color free planet
				root.getChildren().remove(c);
				root.getChildren().add(super.getShape());
			}
		}
	}
	
	
	
	/**
	 * To know if the mouse pointer is inside the planet.
	 * 
	 * @param e The mouse event
	 * @return True if the mouse pointer is inside the planet, else false
	 */
	public boolean isInside(MouseEvent e) {
		double x = e.getX();
		double y = e.getY();
		Circle c = (Circle)super.getShape();
		double cx = (double)c.getCenterX();
		double cy = (double)c.getCenterY();
		double a = Math.abs(cx - x);
		double b = Math.abs(cy - y);
		
		// Pythagore to find distance between mouse and center of circle
		return Math.sqrt(a*a + b*b) <= c.getRadius();
	}
	
	
	
	/**
	 * To know if a point is inside the planet.
	 * 
	 * @param p The point
	 * @return True if the point is inside the planet, else false
	 */
	public boolean isInside(Point2D p) {
		double x = p.getX();
		double y = p.getY();
		Circle c = (Circle)super.getShape();
		double cx = (double)c.getCenterX();
		double cy = (double)c.getCenterY();
		double a = Math.abs(cx - x);
		double b = Math.abs(cy - y);
		
		// Pythagore to find distance between mouse and center of circle
		return Math.sqrt(a*a + b*b) <= c.getRadius();
	}
	
	
	
	/**
	 * To know if a point is inside or near the planet,
	 * with a margin.
	 *  
	 * @param p The point
	 * @param marge The margin added to the radius of the planet
	 * @return True if the point is inside or near the planet, else false
	 */
	public boolean isInside(Point2D p, double marge) {
		double x = p.getX();
		double y = p.getY();
		Circle c = (Circle)super.getShape();
		double cx = (double)c.getCenterX();
		double cy = (double)c.getCenterY();
		double a = Math.abs(cx - x);
		double b = Math.abs(cy - y);
		
		// Pythagore to find distance between mouse and center of circle
		return Math.sqrt(a*a + b*b) <= c.getRadius() + marge;
	}
	
	
	
	/**
	 * To know if the planet is cut off by a line.<br>
	 * 
	 * Works by running a point to the right and determining if it is inside the planet
	 * by using {@link #isInside(Point2D, double)}.
	 * This function is called by {@link SpaceShip#findWayPoint(Circle, Segment)}.
	 * A margin is given to ensure that the ships do not pass too close to the planets.
	 * 
	 * @param l The line that could cut a planet
	 * @return True if the planet is cut off by a line, else false
	 */
	public boolean isCrossed(Line l) {
		double marge = 7;
		Segment s = new Segment(l);
		Point2D p = new Point2D(s.getLine().getStartX(),s.getLine().getStartY());
		
		// while we are not at the end of the segment
		while(!s.isend(p)) {
			if(isInside(p,marge)) {
				return true; // true if we crossed the planet
			}
			else {
				double speed = 5; // speed of the point on the segment
				p = new Point2D(p.getX() + s.getStep(speed).getX(), p.getY() + s.getStep(speed).getY());
			}
		}
		return false;
		
	}
	
	
	
	
	/**
	 * Change all current spaceships with the switch spaceships
	 */
	public void switchSpaceShips() {
		SpaceShip s = spaceships.get(0);
		spaceships.clear();
		
		SpaceShip switchship = s.getSwitchship();
		// set actual ship as switch ship for the switch ship taken
		switchship.setSwitchship(s);
		
		addSpaceship(switchship);
	}
	
	
	
	/**
	 * To know if two planets are equal.
	 * Two planets are equal if they have the same position.
	 * 
	 * @param o Planet type object
	 * @return True if the planets are equal, else false
	 */
	public boolean equals(Object o) {
		if (o != null && o instanceof Planet) {
    		Planet p = (Planet) o;
    		return super.getPosition() == p.getPosition() ; 
    	}
        return false;
	}
	
	
	
	/**
	 * Planet save function.<br>
	 * Elements are saved in this order:
	 * <ul>
	 * <li> The size </li>
	 * <li> The position </li>
	 * <li> The time production </li>
	 * <li> The path of the image resource</li>
	 * <li> The ID of the player owning the planet</li>
	 * <li> The power of the spaceships that the planet is producing </li>
	 * <li> The total number of ships owned (waiting ship and ship sent on mission) </li>
	 * <li> The list of sent and waiting ships</li>
	 * </ul>
	 * @param os Save object
	 * @throws IOException The method potentially contains an exception of type IOException
	 */
	public void save(ObjectOutputStream os) throws IOException {
		os.writeInt(0); // 0 planet circle
		os.writeDouble(super.getSize());
		os.writeObject(super.getPosition());
		os.writeDouble(timeProduction);
		os.writeUTF(super.getPath());
		os.writeInt(currentPlayer);
		os.writeDouble(power);
		os.writeDouble(waveTimer);
		os.writeInt(units);
		
		os.writeInt(spaceships.size());
		for(SpaceShip s : spaceships) {
			s.save(os);
		}
	}
	
	
	
	/**
	 * Sent ships save.
	 * 
	 * @param os Save object
	 * @throws IOException Method capable of launching an exception
	 */
	public void saveShipSent(ObjectOutputStream os) throws IOException {
		os.writeInt(shipSent.size());
		for(SpaceShip s : shipSent) {
			s.save(os);
		}
	}
	
	
	
	
	/**
	 * Function that restores a saved planet.<br>
	 * It first gets the parameters of the planets in order
	 * of which they were saved in {@link #save(ObjectOutputStream)}.<br>
	 * The ships are restored by the planet by making a loop on the number
	 * of ships saved.
	 * 
	 * @param is The backup object
	 * @param root The root of the game (passed as a planet parameter)
	 * @param player1 Passed as a planet parameter
	 * @param player2 Passed as a planet parameter
	 * @param player3 Passed as a planet parameter
	 * @param player4 Passed as a planet parameter
	 * 
	 * @return The planet restored with its number of ships
	 * 
	 * @throws IOException The method potentially contains an exception of type IOException
	 * @throws ClassNotFoundException The method potentially contains an exception of type ClassNotFoundException
	 */
	public Planet load(ObjectInputStream is, Group root, int player1, int player2, int player3, int player4)
			throws IOException, ClassNotFoundException
	{
		// first boolean of inputStream already read by the load function
		double size = is.readDouble();
		Point2D position = (Point2D)is.readObject();
		double timeProduction = is.readDouble();
		String path = is.readUTF();
		int currentPlayer = is.readInt();
		double power = is.readDouble();
		double waveTimer = is.readDouble();
		int units = is.readInt();
		
		// creation of the shape of the planet
		Circle shape = new Circle(position.getX() + size/2, position.getY() + size/2, size/2);
		
		Planet p = new Planet(path,size,shape,root,player1,player2,player3,player4);
		// creation of the shape of the planet
		p.setTimeProduction(timeProduction);
		p.setPlayer(currentPlayer);
		p.setPosition(position);
		p.setPower(power);
		p.setWaveTimer(waveTimer);
		p.setUnits(units);
		
		// ships recovery
		SpaceShip s = new SpaceShip();
		ArrayList<SpaceShip> spaceships = new ArrayList<SpaceShip>();
		
		int nbSpaceship = is.readInt();
		for(int i=0; i<nbSpaceship; i++) {
			s = s.load(is, p, root);
			spaceships.add(s);
		}
		
		p.setSpaceships(spaceships);
		
		return p;
		
	}
	
	
	
	/**
	 * Restores ships on the move to their destination when they are backed up.
	 * 
	 * @param is The restoring object
	 * @param allPlanets List of planets to add their destination planet to the flying ships
	 * @param root Root of the game
	 * @throws IOException  The method potentially contains an exception of type IOException
	 * @throws ClassNotFoundException The method potentially contains an exception of type ClassNotFoundException
	 */
	public void loadShipSent(ObjectInputStream is, ArrayList<Planet> allPlanets, Group root)
			throws IOException, ClassNotFoundException {
		ArrayList<SpaceShip> tabships = new ArrayList<SpaceShip>();
		SpaceShip s = new SpaceShip();
		
		int n = is.readInt();
		for(int i=0; i<n ;i++) {
			SpaceShip ship = s.load(is, this, allPlanets, root);
			tabships.add(ship);
		}
		
		setShipSent(tabships);
	}
	
	
	
	/* ===================================== */
	/* ==========GETTER AND SETTER========== */
	/* ===================================== */
	
	
	
	public Line getLine() {
		return line;
	}

	public int getUnits() {
		return units;
	}

	public int getPlayer() {
		return currentPlayer;
	}

	public void setPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}

	public ArrayList<SpaceShip> getSpaceships() {
		return spaceships;
	}

	public void setSpaceships(ArrayList<SpaceShip> spaceships) {
		this.spaceships = spaceships;
	}

	public boolean isSelectedsrc() {
		return selectedsrc;
	}

	public void setSelectedsrc(boolean selectedsrc) {
		this.selectedsrc = selectedsrc;
	}

	public boolean isSelecteddst() {
		return selecteddst;
	}

	public void setSelecteddst(boolean selecteddst) {
		this.selecteddst = selecteddst;
	}

	public ArrayList<SpaceShip> getShipSent() {
		return shipSent;
	}

	public void setShipSent(ArrayList<SpaceShip> shipSent) {
		this.shipSent = shipSent;
	}

	public void setTimeProduction(double timeProduction) {
		this.timeProduction = timeProduction;
	}
	
	public void setPower(double power) {
		this.power = power;
	}

	public double getCurrentAngle() {
		return currentAngle;
	}

	public void setCurrentAngle(double currentAngle) {
		this.currentAngle = currentAngle;
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}

	public int getPlayer1() {
		return player1;
	}

	public int getPlayer2() {
		return player2;
	}
	
	public int getPlayer3() {
		return player3;
	}

	public int getPlayer4() {
		return player4;
	}

	public void setLine(Line line) {
		this.line = line;
	}

	public double getTimeProduction() {
		return timeProduction;
	}

	public double getPower() {
		return power;
	}

	public Group getRoot() {
		return root;
	}

	public double getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(double currentTime) {
		this.currentTime = currentTime;
	}
	
	public Label getLabel() {
		return label;
	}

	public void setLabel(Label label) {
		this.label = label;
	}

	public ArrayList<Planet> getAllPlanets() {
		return allPlanets;
	}

	public void setWaveTimer(double waveTimer) {
		this.waveTimer = waveTimer;
	}

	public void setUnits(int units) {
		this.units = units;
	}

	public void setAllPlanets(ArrayList<Planet> allPlanets) {
		this.allPlanets = allPlanets;
	}

	public double getWaveTimer() {
		return waveTimer;
	}
	
	
}
