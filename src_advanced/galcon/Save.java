package galcon;

import java.util.ArrayList;

import javafx.scene.Group;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;

/**
 * Allows you to save a game and load a backup.
 * 
 * <ul>
 * <li>
 * Uses the functions {@link Planet#save(ObjectOutputStream)}
 * and {@link Planet#saveShipSent(ObjectOutputStream)}
 * to save a game.
 * </li>
 * <li>
 * Uses the functions {@link Planet#load(ObjectInputStream, Group, int, int, int , int)}
 * and {@link Planet#loadShipSent(ObjectInputStream, ArrayList, Group)}
 * to load a game.
 * </li>
 * <li>
 * At the beginning of the backup, a Boolean is recorded to determine
 * if it is an AI vs IA or human vs IA game to backup.
 * </li>
 * </ul>
 * 
 * @see Planet#save(ObjectOutputStream)
 * @see Planet#saveShipSent(ObjectOutputStream)
 * @see Planet#load(ObjectInputStream, Group, int, int, int , int)
 * @see Planet#loadShipSent(ObjectInputStream, ArrayList, Group)
 */
public class Save {
	
	/**
	 * First player
	 */
	private int player1;
	/**
	 * Second player
	 */
	private int player2;
	/**
	 * Third player
	 */
	private int player3;
	/**
	 * Fourth player
	 */
	private int player4;
	/**
	 * Root of the game
	 */
	private Group root;
	/**
	 * Backup file name
	 */
	private String saveFileName;

	
	
	
	/**
	 * Construct a save object.
	 * 
	 * @param saveFileName Backup file name
	 * @param player1 Player1
	 * @param player2 Player2
	 * @param player3 Player3
	 * @param player4 Player4
	 * @param root Root of the game
	 */
	public Save(String saveFileName, int player1, int player2, int player3, int player4, Group root) {
		this.player1 = player1;
		this.player2 = player2;
		this.player3 = player3;
		this.player4 = player4;
		this.root = root;
		this.saveFileName = saveFileName;
	}
	
	
	
	/**
	 * Saves a current game.
	 * 
	 * @param allPlanets The planets to be saved
	 * @param aiMode The game mode
	 */
	public void saveGame(ArrayList<Planet> allPlanets, boolean aiMode) {
		ObjectOutputStream os = null;

		try {
			final FileOutputStream file = new FileOutputStream(saveFileName);
			os = new ObjectOutputStream(file);
			
			// save the game mode
			os.writeBoolean(aiMode);
			// save the number of planets
			os.writeInt(allPlanets.size());
			// save all planets
			for(Planet p : allPlanets) {
				System.out.println(allPlanets.size()+" "+allPlanets.indexOf(p));
				p.save(os);
			}
			// save all the sent spaceships
			for(Planet p : allPlanets) {
				p.saveShipSent(os);
			}
			os.flush();
	    }
	    catch (final java.io.IOException e) {
	    	e.printStackTrace();
	    }
	    finally {
	    	try {
	    		if (os != null) {
	    			os.flush();
	    			os.close();
	    		}
	    	}
	    	catch (final IOException ex) {
	    		ex.printStackTrace();
	    	}
	    }
	}
	
	
	
	/**
	 * Returns the game mode of the saved game.
	 * 
	 * @return True if it is the AI vs AI mode
	 * 
	 * @throws java.io.IOException The method potentially contains an exception of type IOException
	 */
	public boolean aiMode() throws java.io.IOException {
		
		ObjectInputStream is = null;

		try {
			final FileInputStream file = new FileInputStream(saveFileName);
			is = new ObjectInputStream(file);
			
			return is.readBoolean();
			
		}
		finally {
			try {
				if (is != null) {
					is.close();
				}
			}
			catch (final IOException ex) {
		    ex.printStackTrace();
		  }
		}
		
		
	}
	
	/**
	 * Load a saved game.
	 * 
	 * @return The saved game
	 * 
	 * @throws java.io.IOException The method potentially contains an exception of type IOException
	 */
	public ArrayList<Planet> loadGame() throws java.io.IOException {
		ArrayList<Planet> loadedPlanets = null;

		ObjectInputStream is = null;

		try {
			final FileInputStream file = new FileInputStream(saveFileName);
			is = new ObjectInputStream(file);
			
			// Array to return the loaded planets
			loadedPlanets = new ArrayList<Planet>();
			
			// Empty planets to use the load method
			Planet pload = new Planet();
			Planet psload = new PlanetSquare();
			// skip the aiMode
			is.readBoolean(); 
			// load number of planets to load
			int nbPlanets = is.readInt();
			
			// load all planets in the array
			for(int i=0; i<nbPlanets; i++) {
				
				System.out.println(i); // TODO
				
				int id = is.readInt();
				// load a normal planet
				if(id==0) {
					loadedPlanets.add(pload.load(is, root, player1, player2, player3, player4));
				}
				// load a square planet
				else if(id==1) {
					loadedPlanets.add(psload.load(is, root, player1, player2, player3, player4));
				}
			}
			// load all the sent spaceships
			for(int i=0; i<nbPlanets; i++) {
				loadedPlanets.get(i).loadShipSent(is, loadedPlanets, root);
				// if the planet was sending spaceships it needs all the planets
				if(loadedPlanets.get(i).getUnits() != 0) {
					loadedPlanets.get(i).setAllPlanets(loadedPlanets);
				}
			}
		}
		catch (final ClassNotFoundException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if (is != null) {
					is.close();
				}
			}
			catch (final IOException ex) {
		    ex.printStackTrace();
		  }
		}
		
		return loadedPlanets;
	}
}
