package galcon;

import java.util.ArrayList;

import javafx.scene.Group;
import javafx.scene.input.KeyEvent;

/**
 * <ul>
 * <li>
 * Manages keyboard interactions.<li>
 * The player The player confirms sending ships with SPACE.<li>
 * It increases the number of ships to be sent with Z.<li>
 * It reduces the number of ships to be sent with S.<li>
 * The F5 key saves a game in progress.
 * </ul>
 * 
 */
public class Keyhandler {
	/**
	 * The save class to save a current game
	 */
	private Save save;
	/**
	 * The mode of the game to be registered
	 */
	boolean aiMode;
	
	
	
	/**
	 * Builder of the class, create the backup class.
	 * 
	 * @param saveFile File name for a backup
	 * @param player1 To load an old game with player1
	 * @param player2 To load an old game with player2
	 * @param player3 To load an old game with player3
	 * @param player4 To load an old game with player4
	 * @param root Root of the game to load an old game
	 */
	public Keyhandler(String saveFile, int player1, int player2, int player3, int player4, Group root) {
		this.save = new Save(saveFile, player1, player2, player3, player4, root);
	}
	
	
	
	/**
	 * Method called for the management of keyboard interruptions.
	 * <ul>
	 * <li>
	 *  ESPACE : send all the ships ({@link SpaceShip#isLaunched()}) from the take-off run-way.
	 * <li>
	 * Z : a ship is added.<li>
	 * S : a ship is removed. 
	 * <p>
	 * If there is no ship left
	 * the mission is cancelled, by deselecting the source and destination planets.
	 * </ul>
	 * @param tabPlanets All the planets in the game
	 * @param e The keyboard event
	 * @param root Root of the game
	 */
	public void keyPressed(ArrayList<Planet> tabPlanets, KeyEvent e, Group root){
		switch(e.getCode()) {
		
		case SPACE:
			Planet src = null;
			Planet dst = null;
			// fond source or destination
			for(Planet p : tabPlanets) {
				if(p.isSelecteddst()) {
					dst = p;
				}
				if(p.isSelectedsrc()) {
					src = p;
				}
			}
			// if there is a source and a destination
			if(src!=null && dst!=null) {
				for(Planet p : tabPlanets) {
					if(p.isSelectedsrc()) {
						// we send ships from all sources to the destination
						p.giveMission(dst, tabPlanets);
					}
				}
				// deselect all ships
				for(Planet p : tabPlanets) {
					p.setSelecteddst(false);
					p.setSelectedsrc(false);
					p.deleteLine(root);
				}
			}
			break;
			
		case Z:
			for(Planet p : tabPlanets) {
				if(p.isSelectedsrc()) {
					p.hireUnit(); // adding a ship to the squadron
				}
			}
			break;
		case S:
			boolean cancel = false;
			for(Planet p : tabPlanets) {
				if(p.isSelectedsrc() && p.getUnits() > 0) {
					p.dismissUnit(); // removing a ship from the squadron
				}
				if(p.isSelectedsrc() && p.getUnits() == 0) {
					cancel = true;
				}
			}
			if(cancel) { // cancellation
				for(Planet p : tabPlanets) {
					p.setSelecteddst(false);
					p.setSelectedsrc(false);
					p.deleteLine(root);
				}
			}
			break;
			
		case F5:
			// saving the game in progress
			save.saveGame(tabPlanets, aiMode);
			
		default:
			break;
		}
	}

	
	
	/* ===================================== */
	/* ==========GETTER AND SETTER========== */
	/* ===================================== */
	
	
	
	public Save getSave() {
		return save;
	}

	public void setAiMode(boolean aiMode) {
		this.aiMode = aiMode;
	}

}
