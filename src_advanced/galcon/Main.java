package galcon;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.scene.shape.*;
import javafx.stage.Stage;



/** 
 * <h1> OBJECT PROGRAMMING PROJECT : GALCON </h1>
 * 
 * <h2>Rules of the game:</h2>
 * 
 * <ul>
 * <li>Win when the enemy has no more planets</li>
 * <li>Loose when the player has no more planets</li>
 * </ul>
 * 
 * You have to invade the planets to be able to produce more spaceships
 * 
 * <b> Commands </b>
 * <ul>
 * <li>
 * Keep left click on a possessed planet and drag to the planet to invade<br>
 * If the planet is empty or already taken by the player,
 * the ships are added to the number of ships in the planet<br>
 * If the planet is taken by the enemy, the spaceship deals damage to the ennemy's spaceships.
 * </li>
 * <li>
 * Once the destination planet has been selected,
 * use Z to increase the number of ships to be sent, and S to decrease
 * </li>
 * <li>Press SPACE to confirm the mission</li>
 * </ul>
 * 
 * @see Planet
 * @see SpaceShip
 * @see Menu
 * 
 * @author Thibaut Mandallena
 * @author Alex Kerdudou
 * 
 * @version 1.0
 *
 */
public class Main extends Application {
	
	/**Width of the game window*/
	private final static int WIDTH = 1200;
	
	/**Height of the game window*/
	private final static int HEIGHT = 800;
	
	/**Planet diameter*/
	private final static int SIZEPLANET = 100;
	
	/**Ship diameter*/
	private final static int SIZESPACESHIP = 10;
	
	/**Ship speed*/
	private final static double SPEEDSPACESHIP = 1.3;
	
	/**Speed of enemy action*/
	private final static double ENEMYDELAY = 90;
	
	/**Attack power of the player's ships*/
	private final static double POWERSPACESHIP = 1;
	
	/**Minimum space between each planet
	 * @see Main#generatePlanets(Group, int, int)
	 */
	private final static int MINSPACE = 10;
	/**
	 * Maximum number of spaceships defending free planets
	 */
	private final static int MAXDEFENCE = 3;
	
	/**Production time of the ships for each planet*/
	private final static double TIMEPRODUCTION = 50;
	
	/**Number of times a planet can enter its dedicated space
	 * @see Main#generatePlanets(Group, int, int)
	 */
	private final static double FREESPACE = 1.7;
	/**
	 * Main Home Menu.
	 * If false, the game starts.
	 */
	private boolean ismenu = true;
	
	/**
	 * Enables IA vs IA mode.
	 * If true, keyboard interactions are disabled
	 * Activate the second robots
	 */
	private boolean aiMode = false;
	
	/**First player*/
	private int player1 = 1;
	
	/**Second player*/
	private int player2 = 2;
	
	/**Third player*/
	private int player3 = 3;
	
	/**Fourth player*/
	private int player4 = 4;
	
	/**First enemy*/
	Enemy enemy1;
	
	/**Second enemy*/
	Enemy enemy2;
	
	/**First enemy*/
	Enemy enemy3;
	
	/**Second enemy*/
	Enemy enemy4;
	
	/**Mouse event*/
	private Mousehandler mh;
	
	/**Keyboard event*/
	private Keyhandler kh;
	
	/**List of game planets*/
	private ArrayList<Planet> tabPlanets;
	
	/**Backup file name*/
	private String saveFileName = "src_advanced/save.ser";
	
	
	
	/** Get the name of the path to a resource.
	 * @param name The name of the resource
	 * @return The path to the resource*/
	public static String getRessourcePathByName(String name) {
		return Main.class.getResource('/' + name).toString();
	}
	
	
	
	/** MAIN - Start the game.
	 * 
	 * @param args arguments*/
	public static void main(String[] args) {
		launch(args);
	}
	
	
	
	/** <b>Generating all the planets of the game</b><br>
	 * 
	 * Add an enemy planet and a player planet.<br>
	 * Depending on the size of the game window
	 * and the size of the planets, each planet is given a
	 * generation zone, driven by {@link #MINSPACE}.
	 * {{@link #FREESPACE}} is also taken into account in determining
	 * the division of the planet generation zones.<br>
	 * 
	 * @param root Root of the game
	 * @param nbPlayers The number of computer to play against player1
	 * 
	 * @return The table of all the planets generated
	 */
	public ArrayList<Planet> generatePlanets(Group root, int nbPlayers){
		ArrayList<Planet> tabPlanets = new ArrayList<Planet>();
		
		// determine the window layout
		int cols = WIDTH / (int)Math.round(SIZEPLANET*FREESPACE + MINSPACE*2);
		int rows = HEIGHT / (int)Math.round(SIZEPLANET*FREESPACE + MINSPACE*2);
		// adding planets to the table
		for(int r=0; r < rows; r++) {
			for(int c=0; c < cols; c++) {
				// vary the size of the planets
				double planetSizeCoef = Math.random();
				
				// find random x position - columns - width
				double coefx = Math.random();
				int minx = (WIDTH/cols)*c + MINSPACE + SIZEPLANET / 2;
				int maxx = (WIDTH/cols)*(c+1) - MINSPACE - SIZEPLANET / 2;
				int x = minx + (int)Math.round(coefx*(maxx - minx));
				
				// find random Y position - rows - height
				double coefy = Math.random();
				int miny = (HEIGHT/rows)*r + MINSPACE + SIZEPLANET / 2;
				int maxy = (HEIGHT/rows)*(r+1) - MINSPACE - SIZEPLANET / 2;
				int y = miny + (int)Math.round(coefy*(maxy - miny));
				
				// create planet
				double sizeplanet = SIZEPLANET/2 + planetSizeCoef*(SIZEPLANET/2);
				// size for width of the square
				double squarePlanetSize = sizeplanet/Math.sqrt(2);
				
				// shape for square planets
				Shape rec = new Rectangle(x - squarePlanetSize/2, y - squarePlanetSize/2,
									  squarePlanetSize, squarePlanetSize);
				// shape for regular planets
				Shape circle = new Circle(x, y, sizeplanet/2);
				
				// generate a square or a normal planet
				double choose = Math.random();
				Planet planet;
				if(choose < 0.5) {
					planet = new Planet(getRessourcePathByName("images/planet.png"), sizeplanet, circle, root,
										player1, player2, player3, player4);
					planet.setPosition(new Point2D(x - sizeplanet/2, y - sizeplanet/2));
				}
				else { // Square planets
					planet = new PlanetSquare(getRessourcePathByName("images/planetsquare.png"),
											  squarePlanetSize, rec, root, player1, player2, player3, player4);
					planet.setPosition(new Point2D(x - squarePlanetSize/2, y - squarePlanetSize/2));
				}
				
				tabPlanets.add(planet);
			}
		}
		
		Planet planet;
		Random rand = new Random();
		
		// Select planets and add a spaceship for player1 and player2
		int selectPlayer1 = rand.nextInt(tabPlanets.size());
		int selectPlayer2 = rand.nextInt(tabPlanets.size());
		while(selectPlayer2 == selectPlayer1) {
			selectPlayer2 = rand.nextInt(tabPlanets.size());
		}
		// player1
		planet = tabPlanets.get(selectPlayer1);
		planet.setPlayer(player1);
		// add origin spaceships for player 1
		SpaceShip origin1 = new SpaceShip(getRessourcePathByName("images/spaceship.png"), SIZESPACESHIP,
										  SPEEDSPACESHIP, POWERSPACESHIP, TIMEPRODUCTION, planet, root);
		// create switch ship
		SpaceShip originSwitch1 = new SpaceShip(getRessourcePathByName("images/switchship.png"), SIZESPACESHIP*1.5,
				  								SPEEDSPACESHIP*1.5, POWERSPACESHIP*2.5, TIMEPRODUCTION*0.4, planet, root);
		originSwitch1.setKamikaze(true);
		// add switch ship
		origin1.setSwitchship(originSwitch1);
		// add spaceship
		planet.addSpaceship(origin1);
		// set origin player planet
		tabPlanets.set(selectPlayer1, planet);
		
		// player2
		planet = tabPlanets.get(selectPlayer2);
		planet.setPlayer(player2);
		// add origin spaceships for player2
		SpaceShip origin2 = new SpaceShip(getRessourcePathByName("images/spaceship2.png"), SIZESPACESHIP,
							              SPEEDSPACESHIP, POWERSPACESHIP, TIMEPRODUCTION, planet, root);
		// add spaceship
		planet.addSpaceship(origin2);
		// set origin player planet
		tabPlanets.set(selectPlayer2, planet);
		
		// Select a planet and add a spaceship for player3
		if(nbPlayers >= 2) {
			int selectPlayer3 = rand.nextInt(tabPlanets.size());
			while(selectPlayer3 == selectPlayer1 ||
				selectPlayer3 == selectPlayer2) {
				selectPlayer3 = rand.nextInt(tabPlanets.size());
			}
			
			// player3
			planet = tabPlanets.get(selectPlayer3);
			planet.setPlayer(player3);
			// add origin spaceships for player2
			SpaceShip origin3 = new SpaceShip(getRessourcePathByName("images/spaceship3.png"), SIZESPACESHIP,
								              SPEEDSPACESHIP, POWERSPACESHIP, TIMEPRODUCTION, planet, root);
			// add spaceship
			planet.addSpaceship(origin3);
			// set origin player planet
			tabPlanets.set(selectPlayer3, planet);
			
			// Select a planet and add a spaceship for player4
			if(nbPlayers == 3) {
				int selectPlayer4 = rand.nextInt(tabPlanets.size());
				while(selectPlayer4 == selectPlayer1 ||
					selectPlayer4 == selectPlayer2 ||
					selectPlayer4 == selectPlayer3) {
					selectPlayer4 = rand.nextInt(tabPlanets.size());
				}
				
				// player4
				planet = tabPlanets.get(selectPlayer4);
				planet.setPlayer(player4);
				// add origin spaceships for player2
				SpaceShip origin4 = new SpaceShip(getRessourcePathByName("images/spaceship4.png"), SIZESPACESHIP,
									              SPEEDSPACESHIP, POWERSPACESHIP, TIMEPRODUCTION, planet, root);
				// add spaceship
				planet.addSpaceship(origin4);
				// set origin player planet
				tabPlanets.set(selectPlayer4, planet);
			}
		}
		
		
		
		// adding defense ships to the free planets
		Random randDefence = new Random();
		for(Planet p : tabPlanets) {
			if(p.getPlayer() == 0) {
				SpaceShip defence = new SpaceShip(getRessourcePathByName("images/spaceshipFree.png"),
												  SIZESPACESHIP, 0, POWERSPACESHIP, 0, p, root);
				int nbDefence = randDefence.nextInt(MAXDEFENCE);
				for(int i=0; i<nbDefence; i++) {
					p.addSpaceship(defence);
				}
			}
		}
		
		return tabPlanets;
	} 
	
	
	
	/**
	 * Displays the planets.
	 * 
	 * @see Planet#render(GraphicsContext, Group)
	 * 
	 * @param tabPlanets The list of planets to display
	 * @param gc The graphic context of the game
	 * @param root The root of the game (to take the forms)
	 */
	public void renderPlanets(Collection<Planet> tabPlanets, GraphicsContext gc, Group root) {
		for(Planet planet : tabPlanets) {
			planet.render(gc,root);
		}
	}
	
	
	
	/**
	 * Update of all planets.
	 * 
	 * @see Planet#update()
	 * 
	 * @param tabPlanets The list of planets to update
	 */
	public void updatePlanets(Collection<Planet> tabPlanets) {
		for(Planet planet : tabPlanets) {
			planet.update();
		}
	}
	
	
	
	/**
	 * Determine if a player is a winner.
	 * The game is then stopped and a message of victory, or defeat, is displayed<br>
	 * A player has won when no enemy ships are alive anymore
	 * 
	 * @param player The player to find out if he has won or lost 
	 * @param gc The graphic context of the game
	 * @param root The root of the game
	 * @return true if the player has won
	 */
	public boolean gameOver(int player, GraphicsContext gc, Group root) {
		
		// determine the number of players' planets
		int nbPlayer1 = 0;
		int nbPlayer2 = 0;
		int nbPlayer3 = 0;
		int nbPlayer4 = 0;
		for(Planet p : tabPlanets) {
			for(SpaceShip s : p.getSpaceships()) {
				if(s.getPlayer() == player1) {
					nbPlayer1 ++;
				}
				else if(s.getPlayer() == player2) {
					nbPlayer2 ++;
				}
				else if(s.getPlayer() == player3) {
					nbPlayer3 ++;
				}
				else if(s.getPlayer() == player4) {
					nbPlayer4 ++;
				}
			}
			for(SpaceShip s : p.getShipSent()) {
				if(s.getPlayer() == player1) {
					nbPlayer1 ++;
				}
				else if(s.getPlayer() == player2) {
					nbPlayer2 ++;
				}
				else if(s.getPlayer() == player3) {
					nbPlayer3 ++;
				}
				else if(s.getPlayer() == player4) {
					nbPlayer4 ++;
				}
			}
		}
		// different display depending on the case
		if((player == player1 && nbPlayer2==0 && nbPlayer3==0 && nbPlayer4==0) ||
		   (player == player2 && nbPlayer1==0 && nbPlayer3==0 && nbPlayer4==0) ||
		   (player == player3 && nbPlayer1==0 && nbPlayer2==0 && nbPlayer4==0) ||
		   (player == player4 && nbPlayer1==0 && nbPlayer2==0 && nbPlayer3==0)) {
			String txt = "YOU WIN !";
			gc.strokeText(txt, WIDTH/2, HEIGHT/2);
			gc.fillText(txt, WIDTH/2, HEIGHT/2);
			gc.setTextAlign(TextAlignment.CENTER);
			gc.setTextBaseline(VPos.CENTER);
			removeAllShapes(root);
			return true;
		}
		else if((player == player1 && nbPlayer1==0) ||
				(player == player2 && nbPlayer2==0) ||
				(player == player3 && nbPlayer3==0) ||
				(player == player4 && nbPlayer4==0)){
			String txt = "GAME OVER";
			gc.strokeText(txt, WIDTH/2, HEIGHT/2);
			gc.fillText(txt, WIDTH/2, HEIGHT/2);
			gc.setTextAlign(TextAlignment.CENTER);
			gc.setTextBaseline(VPos.CENTER);
			removeAllShapes(root);
			return true;
		}
		else if(nbPlayer2 == 0 && nbPlayer3 == 0 && nbPlayer4 == 0) {
			String txt = "BOT-1 WIN !";
			gc.strokeText(txt, WIDTH/2, HEIGHT/2);
			gc.fillText(txt, WIDTH/2, HEIGHT/2);
			gc.setTextAlign(TextAlignment.CENTER);
			gc.setTextBaseline(VPos.CENTER);
			removeAllShapes(root);
			return true;
		}
		else if(nbPlayer1 == 0 && nbPlayer3 == 0 && nbPlayer4 == 0) {
			String txt = "BOT-2 WIN !";
			gc.strokeText(txt, WIDTH/2, HEIGHT/2);
			gc.fillText(txt, WIDTH/2, HEIGHT/2);
			gc.setTextAlign(TextAlignment.CENTER);
			gc.setTextBaseline(VPos.CENTER);
			removeAllShapes(root);
			return true;
		}
		else if(nbPlayer1 == 0 && nbPlayer2 == 0 && nbPlayer4 == 0) {
			String txt = "BOT-3 WIN !";
			gc.strokeText(txt, WIDTH/2, HEIGHT/2);
			gc.fillText(txt, WIDTH/2, HEIGHT/2);
			gc.setTextAlign(TextAlignment.CENTER);
			gc.setTextBaseline(VPos.CENTER);
			removeAllShapes(root);
			return true;
		}
		else if(nbPlayer1 == 0 && nbPlayer2 == 0 && nbPlayer3 == 0) {
			String txt = "BOT-4 WIN !";
			gc.strokeText(txt, WIDTH/2, HEIGHT/2);
			gc.fillText(txt, WIDTH/2, HEIGHT/2);
			gc.setTextAlign(TextAlignment.CENTER);
			gc.setTextBaseline(VPos.CENTER);
			removeAllShapes(root);
			return true;
		}
		
		return false;
	}
	
	
	
	/**
	 * Remove the displays from the root of the game
	 *  to display the end of game message.
	 *  
	 * @param root root of the game
	 */
	void removeAllShapes(Group root) {
		for(Planet p : tabPlanets) {
			root.getChildren().remove(p.getShape());
			p.deleteLine(root);
			p.removeLabel(root);
		}
	}
	
	
	
	/**
	 * Main content<br>
	 * Load the background image space.jpg , generates the planets,
	 * initializes mouse and keyboard event.
	 * 
	 * 
	 * @param stage Application of the game
	 * 
	 * @see Keyhandler
	 * @see Mousehandler
	 * 
	 * @see #renderPlanets(Collection, GraphicsContext, Group)
	 * @see #updatePlanets(Collection)
	 */
	public void start(Stage stage) {

		stage.setTitle("GALCON");
		stage.setResizable(false);

		Group root = new Group();
		Scene scene = new Scene(root);
		Canvas canvas = new Canvas(WIDTH, HEIGHT);
		root.getChildren().add(canvas);

		GraphicsContext gc = canvas.getGraphicsContext2D();
		gc.setFont(Font.font("Helvetica", FontWeight.BOLD, 100));
		gc.setFill(Color.DARKBLUE);
		gc.setStroke(Color.YELLOW);
		gc.setLineWidth(2);

		Image space = new Image(getRessourcePathByName("images/space.jpg"), WIDTH, HEIGHT, false, false);
		
		stage.setScene(scene);
		stage.show();
		
		// creation of 2 robots for AI vs AI mode
		Enemy enemy1 = new Enemy(tabPlanets, ENEMYDELAY, root, player1);
		Enemy enemy2 = new Enemy(tabPlanets, ENEMYDELAY, root, player2);
		Enemy enemy3 = new Enemy(tabPlanets, ENEMYDELAY, root, player3);
		Enemy enemy4 = new Enemy(tabPlanets, ENEMYDELAY, root, player4);
		
		// creation of handlers for keyboard and mouse interactions
		kh = new Keyhandler(saveFileName, player1, player2, player3, player4, root);
		mh = new Mousehandler(1); // player control player1
		
		// initialization of the empty planet array
		tabPlanets = new ArrayList<Planet>();
		
		// MENU DISPLAY
		Menu menu = new Menu(root, WIDTH);
		
		// menu button
		Button ai = menu.getAi();
		Button play = menu.getPlay();
		Button load = menu.getLoad();
		Button twoPlayers = menu.getTwoPlayers();
		Button threePlayers = menu.getThreePlayers();
		Button fourPlayers = menu.getFourPlayers();
		Button twoAI = menu.getTwoAi();
		Button threeAI = menu.getThreeAi();
		Button fourAI = menu.getFourAi();
		
			
		ai.setOnAction(ActionEven -> {
			menu.hideMenu();
			menu.showMenuAI();
		});
		// button AI vs AI , launches the game with 2 robots playing face to face
		twoAI.setOnAction(ActionEven -> {
			
			// planets generation
			tabPlanets = generatePlanets(root,1);
			enemy1.setAllPlanets(tabPlanets);
			enemy2.setAllPlanets(tabPlanets);
			enemy3.setAllPlanets(tabPlanets);
			enemy4.setAllPlanets(tabPlanets);
			
			aiMode = true;
			kh.setAiMode(true); // aiMode initialization for a possible backup (F5 key)
			mh = new Mousehandler(-1); //  disables keyboard interactions for the player, only AIs play
			
			ismenu = false;
			menu.hideMenu(); // hides the menu and starts the game
			menu.hideMenuAi();
		});
		// button AI vs AI vs AI , launches the game with 3 robots playing
		threeAI.setOnAction(ActionEven -> {
			
			// planets generation
			tabPlanets = generatePlanets(root,2);
			enemy1.setAllPlanets(tabPlanets);
			enemy2.setAllPlanets(tabPlanets);
			enemy3.setAllPlanets(tabPlanets);
			enemy4.setAllPlanets(tabPlanets);
			
			aiMode = true;
			kh.setAiMode(true); // aiMode initialization for a possible backup (F5 key)
			mh = new Mousehandler(-1); //  disables keyboard interactions for the player, only AIs play
			
			ismenu = false;
			menu.hideMenu(); // hides the menu and starts the game
			menu.hideMenuAi();
		});
		// button 4 AIs, launches the game with 4 robots playing
		fourAI.setOnAction(ActionEven -> {
			
			// planets generation
			tabPlanets = generatePlanets(root,3);
			enemy1.setAllPlanets(tabPlanets);
			enemy2.setAllPlanets(tabPlanets);
			enemy3.setAllPlanets(tabPlanets);
			enemy4.setAllPlanets(tabPlanets);
			
			aiMode = true;
			kh.setAiMode(true); // aiMode initialization for a possible backup (F5 key)
			mh = new Mousehandler(-1); // disables keyboard interactions for the player, only AIs play
			
			ismenu = false;
			menu.hideMenu(); // hides the menu and starts the game
			menu.hideMenuAi();
		});
		
		//button Play
		play.setOnAction(ActionEven -> {
			menu.hideMenu();
			menu.showMenuPlay();
		});
		
		// button 2-Player mode
		twoPlayers.setOnAction(ActionEven -> {

			tabPlanets = generatePlanets(root,1);
			enemy1.setAllPlanets(tabPlanets);
			enemy2.setAllPlanets(tabPlanets);
			enemy3.setAllPlanets(tabPlanets);
			enemy4.setAllPlanets(tabPlanets);
			
			ismenu = false;
			menu.hideMenu();
			menu.hideMenuPlay();
		});
		
		//button 3-Player mode
		threePlayers.setOnAction(ActionEven -> {
			tabPlanets = generatePlanets(root,2);
			enemy1.setAllPlanets(tabPlanets);
			enemy2.setAllPlanets(tabPlanets);
			enemy3.setAllPlanets(tabPlanets);
			enemy4.setAllPlanets(tabPlanets);
			
			ismenu = false;
			menu.hideMenu();
			menu.hideMenuPlay();
		});
		
		//button 3-Player mode
		fourPlayers.setOnAction(ActionEven -> {
			tabPlanets = generatePlanets(root,3);
			enemy1.setAllPlanets(tabPlanets);
			enemy2.setAllPlanets(tabPlanets);
			enemy3.setAllPlanets(tabPlanets);
			enemy4.setAllPlanets(tabPlanets);
			
			ismenu = false;
			menu.hideMenu();
			menu.hideMenuPlay();
		});
		
		// button Load
		load.setOnAction(ActionEven -> {
			try { // try to open the backup file
				tabPlanets = kh.getSave().loadGame(); // get the current game back
				enemy1.setAllPlanets(tabPlanets);
				enemy2.setAllPlanets(tabPlanets);
				enemy3.setAllPlanets(tabPlanets);
				enemy4.setAllPlanets(tabPlanets);
				
				// IA vs IA mode
				if(kh.getSave().aiMode()) {
					aiMode = true;
					kh.setAiMode(true);
					mh = new Mousehandler(-1); // player can't play
				}
				
				ismenu = false;
				menu.hideMenu();
				
			}
			catch (IOException e) {
				
				e.printStackTrace(); // TODO
				
				// alert message if no backup file is found
		        Alert alert = new Alert(AlertType.ERROR);
		        alert.setTitle("Error");
		        alert.setHeaderText(null);
		        alert.setContentText("No saved game found");
		        alert.showAndWait();
			}

		});
		
		menu.showMenu();
		
		scene.setOnMouseDragged(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				mh.mouseDragged(tabPlanets, e, root);
			}
		});
		scene.setOnMouseReleased(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				mh.mouseReleased(tabPlanets, e, root);
			}
		});
		scene.setOnMousePressed(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				mh.mousePressed(tabPlanets, e, root);
			}
		});
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent e) {
				kh.keyPressed(tabPlanets, e, root);
			}
		});
		
		
		/**
		 * Main display loop.
		 * <ul>
		 * <li> Rendering of the background </li>
		 * <li> Rendering of planets </li>
		 * <li> Update of the planets</li>
		 * </ul>
		 */
		new AnimationTimer() {
			public void handle(long arg0) {
				gc.drawImage(space, 0, 0);
				
				// until the game is finished (with the gameOver function) and the menu is hidden
				// we move forward in the game
				
				if(!ismenu && !gameOver(mh.getPlayer(), gc, root)){
					// refreshing the rendering of the planets
					renderPlanets(tabPlanets, gc, root);
					// update of the planets
					updatePlanets(tabPlanets);
					
					if(enemy2.getEnemy().size() != 0) {
						enemy2.update(); // enemies updates
					}
					if(enemy3.getEnemy().size() != 0) {
						enemy3.update();
					}
					if(enemy4.getEnemy().size() != 0) {
						enemy4.update();
					}
					if(aiMode && enemy1.getEnemy().size() != 0) {
						enemy1.update(); // robot update for ai mode
					}
				}
			}
		}.start();
	}
	
}
