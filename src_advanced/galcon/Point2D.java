package galcon;

/**
 * Create a coordinate point (x,y).
 *
 */
public class Point2D implements java.io.Serializable {
	
	/**
	 * ID for serialization
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Coordinates ,x for the abscissa and y for the ordinate
	 */
	private double x, y;
	/**
	 * Name 
	 */
	private String name;
	
	
	
	/**
	 * Construct a point.
	 * 
	 * @param x x-coordinate
	 * @param y y-coordinate
	 */
	public Point2D(double x, double y) {
		this.x = x;
		this.y = y;
		name = "Point";
	}
	
	
	
	/**
	 * Method of displaying a point.
	 * @return String representing a point
	 */
    public String toString() {
        return name +"<" + x + ", "  + y + ">";
    }

	
	
	/**
	 * Calculating the distance between two points.
	 * @param p A point
	 * @return The distance between two points
	 */
	public double distance(Point2D p) {
		double d1 = p.getX() - x;
		double d2 = p.getY() - y;
		return Math.sqrt(d1*d1 + d2*d2);
	}
	
	
	
	/**
	 * Checks if two points are equal (same coordinates).
	 * @param o Point type object
	 * @return True if the two points are equal, else false
	 */
	public boolean equals(Object o) {
    	if (o != null && o instanceof Point2D) {
    		Point2D p = (Point2D) o;
    		return x == p.getX() && y == p.getY();
    	}
        return false;
    }
	
	
	
	/**
	 * Create a copy of the point.
	 * @return The copy of the point
	 */
	public Point2D copy() {
		return new Point2D(this.x, this.y);
	}
	
	
	
	/* ===================================== */
	/* ==========GETTER AND SETTER========== */
	/* ===================================== */
	
	
	

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
}
