<p align="center">
<img alt ="galcon" src="https://zupimages.net/up/20/36/1zie.png"/>
</p>
Realization of the real-time strategy video game [Galcon](https://en.wikipedia.org/wiki/Galcon) for the Object-oriented programming course project of Licence 3 of the University of Bordeaux.
<p align="center">
<img alt ="galcon" src="https://media.giphy.com/media/LMngwWuNh9cueEOpcq/giphy.gif" height="400"/>
</p>

# Run

### Requirements

 - Amazon Corretto 8 JDK ( or other jdk with java 8 or more and built-in javafx )

### Build & Run

From src_advanced folder in the terminal :
```shell
mkdir ../bin/
```
```shell
javac -d ../bin/ galcon/*java
```
```shell
cp -r ../resources/* ../bin/
```
```shell
cd ../bin/
```
```shell
java galcon.Main
```
### Run from JAR 

Download [JAR File](https://gitlab.com/Seynshun/galcon/-/blob/master/Galcon-master.jar)

In the terminal :
```shell
java -jar Galcon-master.jar
```

# Commands

|  Action|Keyboard key  |
|--|--|
| Save the game | F5 |
| Increase spaceships to send | Z |
| Decrease spaceships to send | S |
| Send spaceships | SPACE |
| Kamikaze spaceships | SHIFT + LEFT MOUSE|
| Sending spacesships from all your planets | CTRL + LEFT MOUSE |

##  How the game works

### Basics

When you are playing, you have to click on a planet and drag the mouse until the planet you want to send spaceships on. Then the source planet is automatically hiring one spaceship for the mission. You can add more using Z key or you can remove some using S. Then use SPACE to send your spaceships.

You can also select more than one source of your planets !
First select a source planet, then drag the mouse on the planet you want to use as other source,
then drag it to the destination.

A spaceship arriving in a new planet will either add the spaceship to the planet if it is empty or owned, or deal as much damage as it has power to the spaceships power of the enemy planet, and when the enemy spaceship doesn't have any power left it is destroyed

Free planets can have few spaceships to defend themselves. You need to destroy them before colonizing the planet.
Every planets you own are producing spaceships, as well for the enemies planets.

### Advanced techniques

There is square planets ! They reduce the time production of your spaceship by two but they also make your spaceships take twice more damage, so you can easily produce a lot of spaceships but they are vulnerable if they stay too long on a square planet.

You can choose to use different type of spaceships. When pressing SHIFT and left clicking on a planet you
own it will destroy all your old spaceships to replace them with a special spaceship.
This second type of spaceship is a kamikaze. It can't invade other planets, but it reproduces a lot faster
and has twice more power. It resists twice more than normal spaceships to attacks and deals twice more damage.
But to invade planets you need regular spaceships, you can switch back to normal spaceship, destroying all your
previous kamikaze spaceships, using SHIFT and left click again on the planet.

You can use CTRL click to select all your planets as source to send a lot of spaceships to one destination easily.

# Game modes

###  Player VS AI
You can choose to play against 1, 2 or 3 enemies.

### AI vs AI
You can choose to watch 2, 3 or 4 robots fighting each-other.
